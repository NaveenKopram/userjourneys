package utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.influxdb.dto.Point;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.Reporter;

import commonFunctions.TestUtil;

public class InfluxDBListener implements ITestListener {
	DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	Date date = new Date();
	String currentDate = dateFormat.format(date).toString();

	@Override
	public void onTestStart(ITestResult iTestResult) {

    }

	@Override
    public void onTestSuccess(ITestResult iTestResult) {
        this.postTestMethodStatus(iTestResult, "PASS");
    }

	@Override
    public void onTestFailure(ITestResult iTestResult) {
        this.postTestMethodStatus(iTestResult, "FAIL");
    }

	@Override
    public void onTestSkipped(ITestResult iTestResult) {
        this.postTestMethodStatus(iTestResult, "SKIPPED");
    }

	@Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {

    }

	@Override
    public void onStart(ITestContext iTestContext) {

    }

	@Override
    public void onFinish(ITestContext iTestContext) {
//        this.postTestClassStatus(iTestContext);
    }

	private void postTestMethodStatus(ITestResult iTestResult, String status) {
		String Module = Reporter.getCurrentTestResult().getTestContext().getCurrentXmlTest().getParameter("Module");
		String Env = Reporter.getCurrentTestResult().getTestContext().getCurrentXmlTest().getParameter("Env");
		System.out.println(Env);
		Point point = Point.measurement("testCases").time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
				.tag("ModuleTag",Module)
				.addField("ModuleField",Module)
				
				.tag("EnvironmentTag",Env)
				.addField("EnvironmentField",Env)
				
				.tag("testclassTag", iTestResult.getTestClass().getName())
				.addField("testclassField", iTestResult.getTestClass().getName())
			
				.tag("ResponseTimeTag", Long.toString(TestUtil.response.getTimeIn(TimeUnit.MILLISECONDS)))
				.addField("ResponseTimeField", Long.toString(TestUtil.response.getTimeIn(TimeUnit.MILLISECONDS)))
				
				.tag("ResponseCodeTag", Long.toString(TestUtil.response.getStatusCode()))
				.addField("ResponseCodeField", Long.toString(TestUtil.response.getStatusCode()))
				
				.tag("DescriptionTag", iTestResult.getMethod().getDescription())
				.addField("DescriptionField", iTestResult.getMethod().getDescription())
				
				.tag("ResultTag", status)
				.addField("ResultField", status).build(); 
		UpdateResults.post(point);
	}

	private void postTestClassStatus(ITestContext iTestContext) {
		Point point = Point.measurement("testclass").time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
				.tag("name", iTestContext.getAllTestMethods()[0].getTestClass().getName())
				.addField("duration", (iTestContext.getEndDate().getTime() - iTestContext.getStartDate().getTime()))
				.build();
		UpdateResults.post(point);
	}
}