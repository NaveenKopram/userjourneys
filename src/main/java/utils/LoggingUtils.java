package utils;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import rp.com.google.common.io.BaseEncoding;

import java.io.File;

/**
 * Useful for binary data (e.g. sending files to ReportPortal)
 *
 * @author Andrei Varabyeu
 */
public class LoggingUtils {

    private static final Logger LOGGER = LogManager.getLogger("Root_Logger");

//    private LoggingUtils() {
//        //statics only
//    }

    public static void info(String message) {
        LOGGER.info("{}", message);
    }
    
    public static void error(String message) {
        LOGGER.error("{}", message);
    }

//    public static void log(byte[] bytes, String message) {
//        LOGGER.info("RP_MESSAGE#BASE64#{}#{}", BaseEncoding.base64().encode(bytes), message);
//    }

    public static void logBase64(String base64, String message) {
        LOGGER.info("RP_MESSAGE#BASE64#{}#{}", base64, message);
    }
    
    public static void log(File file, String message) {
        LOGGER.info("RP_MESSAGE#FILE#{}#{}", file.getAbsolutePath(), message);
    }
}
