package commonFunctions;

import static io.restassured.RestAssured.given;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.ott.RequestApiEngine.PostLoginUserReqWithMobileAndPassword;
import com.ott.RequestApiEngine.PostRegisteredUserReq;
import com.ott.RequestApiEngine.PostWhLoginUserReqWithEmailAndPassword;
import com.ott.RequestApiEngine.PostWhLoginUserReqWithMobileAndPassword;


import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.config.EncoderConfig;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.FilterableRequestSpecification;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import resources.APIResources;
import resources.JsonResources;
import utils.LoggingUtils;

public class TestUtil{
	
	protected String BaseUrl_getCountry="BaseUrl_getCountry";
	protected String BaseUrl_getPlatformToken="BaseUrl_getPlatformToken";
	protected String BaseUrl_postLanguage="BaseUrl_postLanguage";
	protected String BaseUrl_postCountryList="BaseUrl_postCountryList";
	protected String BaseUrl_zee_RegisteredUser="BaseUrl_zee_RegisteredUser";
	protected String BaseUrl_getCurrentUserJson="BaseUrl_getCurrentUserJson";
	protected String BaseUrl_subscriptionAPILogin="BaseUrl_subscriptionAPILogin";
	protected String BaseUrl_subscriptionAPI="BaseUrl_subscriptionAPI";
	protected String BaseUrl_apiDocs="BaseUrl_apiDocs";
	protected String BaseUrl_userAPI="BaseUrl_userAPI";
	protected String BaseUrl_catalogAPI="BaseUrl_catalogAPI";
	protected String BaseUrl_b2bAPI="BaseUrl_b2bAPI";
    protected String BaseUrl_b2bpartnersubAPI="BaseUrl_b2bpartnersubAPI";
	protected String BaseUrl_gwAPI="BaseUrl_gwAPI";
	protected String BaseUrl_whAPI="BaseUrl_whAPI";
	protected String BaseUrl_wh_dekeyAPI="BaseUrl_wh_dekeyAPI";
	protected String BaseUrl_paymentsAPI="BaseUrl_paymentsAPI";
	protected String BaseUrl_userActionAPI="BaseUrl_userActionAPI";
	protected String BaseUrl_partnerintAPI="BaseUrl_partnerintAPI";
	protected String BaseUrl_optimoveAPI="BaseUrl_optimoveAPI";
	protected String BaseUrl_optimove_microsegmentAPI="BaseUrl_optimove_microsegmentAPI";
	protected String BaseUrl_pwaStoriesAPI="BaseUrl_pwaStoriesAPI";
	protected String BaseUrl_apiGatewayAPI="BaseUrl_apiGatewayAPI";
    protected String BaseUrl_singlePlaybackAPI="BaseUrl_singlePlaybackAPI";
    protected String BaseUrl_recoAPI="BaseUrl_recoAPI";
    protected String BaseUrl_userchoiceAPI="BaseUrl_userchoiceAPI";
    protected String BaseUrl_ContentAPI="BaseUrl_ContentAPI";
    protected String BaseUrl_launchAPI="BaseUrl_launchAPI"; 
    protected String BaseUrl_whAPI_pnode="BaseUrl_whAPI_pnode"; 
    protected String BaseUrl_CountryXtraAPI="BaseUrl_CountryXtraAPI"; 
    
    

    
	public static Response response;
	public static FilterableRequestSpecification req;
	
	public static ExtentHtmlReporter htmlReporter;
	public static ExtentReports extent;
	public static ExtentTest testLogger;

	//private static RequestSpecification req;
	private ResponseSpecification res;
	//private Response response;
	public static String path=System.getProperty("user.dir");
	public static String os = System.getProperty("os.name").toLowerCase();
	public static String env;
	
	@BeforeSuite
	public void loggigng() throws FileNotFoundException {
		PrintStream log =new PrintStream(new FileOutputStream("logging.txt"));
		RestAssured.filters(new RequestLoggingFilter().logRequestTo(log), new ResponseLoggingFilter().logResponseTo(log));
		env = Reporter.getCurrentTestResult().getTestContext().getCurrentXmlTest().getParameter("Env");
	}
	
	public RequestSpecification requestSpecification(String baseUrl) throws IOException
	{		
		req=(FilterableRequestSpecification) new RequestSpecBuilder().setUrlEncodingEnabled(false).setRelaxedHTTPSValidation().setBaseUri(getGlobalValue(baseUrl))
				.setContentType(ContentType.JSON).build();
		return req;		
	}
	/*
	 * public RequestSpecification requestSpecification1(String baseUrl) throws
	 * IOException { req=(FilterableRequestSpecification) new
	 * RequestSpecBuilder().setRelaxedHTTPSValidation().setBaseUri(getGlobalValue(
	 * baseUrl))
	 * .setConfig(RestAssured.config().encoderConfig(EncoderConfig.encoderConfig().
	 * encodeContentTypeAs("x-www-form-urlencoded",ContentType.URLENC))).build();
	 * return req; }
	 */
	
	public RequestSpecification requestSpecification1(String baseUrl) throws IOException
	{		
		req=(FilterableRequestSpecification) new RequestSpecBuilder().setUrlEncodingEnabled(true).setRelaxedHTTPSValidation().setBaseUri(getGlobalValue(baseUrl))
				.build();
		return req;		 
	}
	
	public ResponseSpecification responseSpecification(int statusCode) {
		res=new ResponseSpecBuilder().expectStatusCode(statusCode).build();
		return res;
		
	}
	public String getGlobalValue(String key) throws IOException
	{
		Properties prop =new Properties();
		FileInputStream fis =new FileInputStream(env + ".properties");
		prop.load(fis);
		return prop.getProperty(key);		
	}
	
	public static String getJsonPath(Response response, String key)
	{
		  String resp=response.body().asString();
		JsonPath   js = new JsonPath(resp);
		return js.get(key).toString();
	}
	
	
   public Response getJsonFromAPIDoc(String resource) throws IOException {
	     RequestSpecification request1 = given().spec(requestSpecification(BaseUrl_apiDocs));
	     Response response1 = request1.when().get(getJsonResource(resource));
		 return response1;
   }
	
	public String getResource(String resource) {
		APIResources resourceAPI=APIResources.valueOf(resource);
		return resourceAPI.getResource();
	}
	
	public String getJsonResource(String resource) {
		JsonResources resourceJson=JsonResources.valueOf(resource);
		return resourceJson.getResource();
	}
	
	public void compareJsonSchemaAndResponseBody(Map<String, String> mapAPI, String jsonResource, String responsecode, String apiResource, SoftAssert softasrt) throws IOException {	
		Response response=getJsonFromAPIDoc(jsonResource);
		//System.out.println("here-"+response.asString());
		compareHashMap(mapAPI, response, responsecode, apiResource, softasrt);
	}

	public void compareJsonSchemaAndResponseBodyArray(Map<String, String> mapAPI, String jsonResource, String responsecode, String apiResource, SoftAssert softasrt) throws IOException {	
		Response response=getJsonFromAPIDoc(jsonResource);
		//System.out.println("here-"+response.asString());
		compareArrayHashMap(mapAPI, response, responsecode, apiResource, softasrt);
	}
	
	public JsonPath getdocJson(Response response) {
		JsonPath docJson = response.jsonPath();
		return docJson;
	}
	
	public Map<String, String> convertAPIResponseToHashMap(Response response) {
		JsonPath APIJson = response.jsonPath();
		 Map<String, String> mapAPI = APIJson.getMap("");
		 
		 return mapAPI;
	}
	
	public Map<String, String> convertArrayAPIResponseToHashMap(Response response) {
		JsonPath APIJson = response.jsonPath();
		 Map<String, String> mapAPI = APIJson.getMap("array[0]");
		 return mapAPI;
	}
	
	public Map<String, String> convertListAPIResponseToHashMap(Response response) {
		JsonPath APIJson = response.jsonPath();
		 List <Map<String, ?>> ResponseArrayAPI = APIJson.get();
		 System.out.println(ResponseArrayAPI.get(0).toString());
		 //Map<String, String> mapAPI = (Map<String, String>) ResponseArrayAPI.get(0);
		 //System.out.println(mapAPI.size());
		  
//		List mapListAPI = APIJson.get("array");
//		System.out.println("API List:" + mapListAPI);
//		System.out.println("API Size:" + mapListAPI.size());
		Map<String, String> mapAPI = null;
		for(int i = 0; i < ResponseArrayAPI.size(); i++) {
			mapAPI = (Map<String, String>) ResponseArrayAPI.get(i);
		}
		System.out.println(mapAPI);
		 //Map<String, String> mapAPI = APIJson.getMap("array[0]");
		 return mapAPI;
	}
	
	//not working when we have path Params in the url
	public String getPath_resource(Response response) {
		JsonPath docJson = response.jsonPath();
		 String pathArray = docJson.get("paths").toString();	
	     String [] newpath = pathArray.split("\\{", 2);
	     String [] path = newpath[1].split("=");
	     String path_resource=path[0];   	
	     return path_resource;
	}
	
	
	public String getMethod(Response response, String ResourceName) {
		System.out.println("Inside getmethod");
		JsonPath docJson = response.jsonPath();	
		String pathVar;
		//		 System.out.println("paths." + "\"" + getResource(ResourceName) + "\"");
		   if(getResource(ResourceName).contains("-")) {
			   pathVar = "\'" + getResource(ResourceName) + "\'";
		   }else {
			   pathVar = "\"" + getResource(ResourceName) + "\"";
		   };
		 String callArray = docJson.get("paths." + pathVar).toString();
	     System.out.println("callArray-"+callArray);
	     String [] newcall = callArray.split("\\{", 2);
	     String [] call = newcall[1].split("=");
	   
	     String type = call[0];
	     System.out.println("method-"+type);
	     return type;
	}
	//fetching content type
	public String getContentMethod(Map<String, String> mapAPI) {
		System.out.println("Inside getContentMethod");
		String content = mapAPI.toString();
	     String [] newcall = content.split("\\{", 2);
	     String [] call = newcall[1].split("=");
	   //Getting content_type
	     String contentType =call[0];
	     System.out.println("content" + contentType);
	     //System.out.println("ContentType-"+contentType);
	     return contentType;
	}
	
	public static class TestDataBuild {
		public  PostWhLoginUserReqWithEmailAndPassword getWhPostRegisteredEmailUserReq(String email, String password,String guest_token,String platform,String device,String aid,String lotame_cookie_id,String version) {
			PostWhLoginUserReqWithEmailAndPassword ru=new PostWhLoginUserReqWithEmailAndPassword(email, password,guest_token,platform,device,aid,lotame_cookie_id,version);
			return ru;
		}
		
		public  PostWhLoginUserReqWithMobileAndPassword getWhPostRegisteredMobileUserReq(String mobile, String password,String guest_token,String platform,String device,String aid,String lotame_cookie_id,String version) {
			PostWhLoginUserReqWithMobileAndPassword ru=new PostWhLoginUserReqWithMobileAndPassword(mobile, password,guest_token,platform,device,aid,lotame_cookie_id,version);
			return ru;
		}
			public static  PostLoginUserReqWithMobileAndPassword getPostLoginUserReqWithMobileAndPassword(String mobile, String password) {
				PostLoginUserReqWithMobileAndPassword ru=new PostLoginUserReqWithMobileAndPassword(mobile, password);
				return ru;
		} 
			public static  PostRegisteredUserReq getPostRegisteredUserReq(String email, String password) {
				PostRegisteredUserReq ru=new PostRegisteredUserReq(email, password);
				return ru;
			}
			
	}
	
	public String getAuthorizationForMobileLogin(String username, String password) throws IOException {
		req = (FilterableRequestSpecification) given().spec(requestSpecification(BaseUrl_subscriptionAPILogin))
				.body(TestDataBuild.getPostLoginUserReqWithMobileAndPassword(username, password));
		response = req.when().post(getResource("subscription_login_getauthorization_mobile_path")).then().assertThat().spec(responseSpecification(200)).extract()
				.response();
		String loginuserRespstring= response.body().asString();
		System.out.println("Post Fetch GuestToken API Response" +loginuserRespstring);
		String access_token = getJsonPath(response, "access_token");
		String token_type = getJsonPath(response, "token_type");
		String authorization = token_type.toLowerCase() + " " + access_token;	
		return authorization;
		
	}
	
	public String getAuthorizationForEmailLogin(String username, String password) throws IOException {
		req = (FilterableRequestSpecification) given().spec(requestSpecification(BaseUrl_subscriptionAPILogin))
				.body(TestDataBuild.getPostRegisteredUserReq(username, password));
		response = req.when().post(getResource("subscription_login_getauthorization_email_path")).then().assertThat().spec(responseSpecification(200)).extract()
				.response();
		String loginuserRespstring= response.body().asString();
		System.out.println("Post Fetch GuestToken API Response" +loginuserRespstring);
		String access_token = getJsonPath(response, "access_token");
		String token_type = getJsonPath(response, "token_type");
		String authorization = token_type.toLowerCase() + " " + access_token;
		return authorization;
		}

	
	public Map<String, String> getSchemaMapping(Response response, String responseCode, String ResourceName) throws IOException {
		 JsonPath docJson = response.jsonPath();
		 String method=getMethod(response, ResourceName);
	     System.out.println("responseCode-"+responseCode);
		 String pathVar;
//		 System.out.println("paths." + "\"" + getResource(ResourceName) + "\"");
		 if(getResource(ResourceName).contains("-")) {
		   pathVar = "\'" + getResource(ResourceName) + "\'";
		 }else {
		   pathVar = "\"" + getResource(ResourceName) + "\"";
		 }
	     String ResponseArray2 = docJson.get("paths." + pathVar + "." +method+".responses['" + responseCode + "'].content['application/json'].schema.$ref").toString();
		 System.out.println(ResponseArray2);
		 String newstr = ResponseArray2.replaceAll("\\s", ""); 
		 String [] schemaNameArray = newstr.split("schemas/");
		 String schemaName = schemaNameArray[1];
		 System.out.println(schemaName);
		 RestAssured.baseURI = newstr;
		 RequestSpecification httpRequest2 = RestAssured.given();
		 Response response2 = httpRequest2.request(Method.GET, newstr);
		 System.out.println(response2.getStatusCode());
	     JsonPath newData2 = response2.jsonPath();
	     Map<String, String> map = newData2.get("components.schemas." + schemaName +".properties");
	     System.out.println(map);
	     return map;
	}
	
	public void compareHashMap(Map<String, String> mapAPI, Response response, String responseCode, String ResourceName, SoftAssert softasrt) throws IOException {
		JsonPath docJson = response.jsonPath();
	     System.out.println("responseCode-"+responseCode);
	     String method=getMethod(response, ResourceName);
		 String pathVar;
//		 System.out.println("paths." + "\"" + getResource(ResourceName) + "\"");
		 if(getResource(ResourceName).contains("-")) {
		   pathVar = "\'" + getResource(ResourceName) + "\'";
		 }else {
		   pathVar = "\"" + getResource(ResourceName) + "\"";
		 }
	     
//	     System.out.println("ContentType-"+contentType);
		 Map<String, String> content = docJson.getMap("paths." + pathVar + "." +method+".responses['" + responseCode + "'].content");
		 String contentType=getContentMethod(content);
		 System.out.println("ResponseArray" + contentType);
	     System.out.println("paths." + pathVar + "." +method+".responses['" + responseCode + "'].content['"+contentType+"'].schema.$ref");
	     String ResponseArray2 = docJson.get("paths." + pathVar + "." +method+".responses['" + responseCode + "'].content['"+contentType+"'].schema.$ref").toString();
		 System.out.println("ResponseArray2-"+ResponseArray2);
		 String newstr = ResponseArray2.replaceAll("\\s", ""); 
		 String [] schemaNameArray = newstr.split("schemas/");
		 String schemaName = schemaNameArray[1];
		 System.out.println("schemaName : "+schemaName);
		 RequestSpecification httpRequest2 = RestAssured.given();
		 Response response2 = httpRequest2.request(Method.GET, newstr);
	     JsonPath newData2 = response2.jsonPath();
	     Map<String, String> mapSchema = newData2.get("components.schemas." + schemaName +".properties");
	     System.out.println(schemaName);
	     System.out.println(mapSchema);
		 Iterator hmIteratorSchema = mapSchema.entrySet().iterator(); 
	     Iterator hmIteratorAPI = mapAPI.entrySet().iterator(); 

	     while (hmIteratorSchema.hasNext() || hmIteratorAPI.hasNext()) { 
	          Map.Entry mapElementSchema = (Map.Entry)hmIteratorSchema.next(); 
	          Map.Entry mapElementAPI = (Map.Entry)hmIteratorAPI.next(); 
	          
	          if(mapElementSchema.getKey().equals("additional")) {
	          		continue;
	          }else {
	          	if((mapElementSchema.getKey().toString()).equalsIgnoreCase(mapElementAPI.getKey().toString())) {
		            	System.out.println("JSON Doc ID: " + mapElementSchema.getKey() + " matched with API ID: " + mapElementAPI.getKey());
		            	softasrt.assertEquals((mapElementSchema.getKey().toString()).equalsIgnoreCase(mapElementAPI.getKey().toString()), true, "JSON Doc ID: " + mapElementSchema.getKey() + " matched with API ID: " + mapElementAPI.getKey());
		            	String dataType = newData2.get("components.schemas." + schemaName + ".properties." + mapElementSchema.getKey() + ".type").toString();
		            	System.out.println(dataType);
		            	
		            	String objType = mapElementAPI.getValue().getClass().getSimpleName();
		            	
		            	if(objType.equalsIgnoreCase(dataType)==false) {
		            		dataType = objType;
		            	}
//		            	if(dataType.equalsIgnoreCase("number")) {
//		            		dataType = "Integer";
//		            	}else if(dataType.equalsIgnoreCase("array")) {
//		            		dataType = "ArrayList";
//		            	}
		            	
	
		            	if(objType.equalsIgnoreCase(dataType)) {
		            		System.out.println("Correct Data type is displayed for the key: " + mapElementSchema.getKey());
		            		testLogger.log(Status.PASS,
									MarkupHelper.createLabel("Correct Data type is displayed for the key: " + mapElementSchema.getKey(), ExtentColor.GREEN));
		            		LoggingUtils.info("Correct Data type is displayed for the key: " + mapElementSchema.getKey());
		            		softasrt.assertEquals(objType.equalsIgnoreCase(dataType), true, "Correct Data type is displayed for the key: " + mapElementSchema.getKey());
							
		            	}else{
		            		System.out.println("Difference in data type for key: " + mapElementSchema.getKey() + " Schema Data Type is: " + dataType + " API Data Type is: " + objType);
		            		testLogger.log(Status.FAIL,
									MarkupHelper.createLabel("Difference in data type for key: " + mapElementSchema.getKey() + " Schema Data Type is: " + dataType + " API Data Type is: " + objType, ExtentColor.RED));
		            		LoggingUtils.error("Difference in data type for key: " + mapElementSchema.getKey() + " Schema Data Type is: " + dataType + " API Data Type is: " + objType);
		            		softasrt.assertEquals(false, true, "Difference in data type for key: " + mapElementSchema.getKey() + " Schema Data Type is: " + dataType + " API Data Type is: " + objType);
							}
	          	}else{
	          		System.out.println("Differences Found: JSON Doc Key is: " + mapElementSchema.getKey() + " and API Key is: " + mapElementAPI.getKey());
	          		testLogger.log(Status.FAIL,
							MarkupHelper.createLabel("Differences Found: JSON Doc Key is: " + mapElementSchema.getKey() + " and API Key is: " + mapElementAPI.getKey(), ExtentColor.RED));
	          		LoggingUtils.error("Differences Found: JSON Doc Key is: " + mapElementSchema.getKey() + " and API Key is: " + mapElementAPI.getKey());
	          		softasrt.assertEquals(false, true, "Differences Found: JSON Doc Key is: " + mapElementSchema.getKey() + " and API Key is: " + mapElementAPI.getKey());
					}
	          }
	     }
	}  

	public void compareArrayHashMap(Map<String, String> mapAPI, Response response, String responseCode, String ResourceName, SoftAssert softasrt) throws IOException {
		JsonPath docJson = response.jsonPath();
	     System.out.println("responseCode-"+responseCode);
	     String method=getMethod(response, ResourceName);
		 String pathVar;
//		 System.out.println("paths." + "\"" + getResource(ResourceName) + "\"");
		 if(getResource(ResourceName).contains("-")) {
		   pathVar = "\'" + getResource(ResourceName) + "\'";
		 }else {
		   pathVar = "\"" + getResource(ResourceName) + "\"";
		 }
	     
//	     System.out.println("ContentType-"+contentType);
		 Map<String, String> content = docJson.getMap("paths." + pathVar + "." +method+".responses['" + responseCode + "'].content");
		 String contentType=getContentMethod(content);
		 System.out.println("ResponseArray" + contentType);
	     System.out.println("paths." + pathVar + "." +method+".responses['" + responseCode + "'].content['"+contentType+"'].schema.$ref");
	     String ResponseArray2 = docJson.get("paths." + pathVar + "." +method+".responses['" + responseCode + "'].content['"+contentType+"'].schema.$ref").toString();
		 System.out.println("ResponseArray2-"+ResponseArray2);
		 String newstr = ResponseArray2.replaceAll("\\s", ""); 
		 String [] schemaNameArray = newstr.split("schemas/");
		 String schemaName = schemaNameArray[1];
		 System.out.println("schemaName : "+schemaName);
		 RequestSpecification httpRequest2 = RestAssured.given();
		 Response response2 = httpRequest2.request(Method.GET, newstr);
	     JsonPath newData2 = response2.jsonPath();
	     Map<String, String> mapSchema1 = newData2.getMap("components.schemas." + schemaName + ".items");
	     Iterator hmIteratorSchema1 = mapSchema1.entrySet().iterator();
	     Map<String, Map<String, ?>> mapSchema = null;
	     while (hmIteratorSchema1.hasNext()) { 
	    	 Map.Entry mapElementSchema1 = (Map.Entry)hmIteratorSchema1.next();  
	    	 if(mapElementSchema1.getKey().equals("properties")) {
		    	System.out.println(mapElementSchema1.getKey().toString());
		    	mapSchema = (Map<String, Map<String, ?>>) mapElementSchema1.getValue();
	            System.out.println(mapSchema); 
	    	 }

	     }

		 Iterator hmIteratorSchema = mapSchema.entrySet().iterator(); 
	     Iterator hmIteratorAPI = mapAPI.entrySet().iterator(); 

	     while (hmIteratorSchema.hasNext() || hmIteratorAPI.hasNext()) { 
	          Map.Entry mapElementSchema = (Map.Entry)hmIteratorSchema.next(); 
	          Map.Entry mapElementAPI = (Map.Entry)hmIteratorAPI.next(); 
	          
	          if(mapElementSchema.getKey().equals("additional")) {
	          		continue;
	          }else {
	          	if((mapElementSchema.getKey().toString()).equalsIgnoreCase(mapElementAPI.getKey().toString())) {
		            	System.out.println("JSON Doc ID: " + mapElementSchema.getKey() + " matched with API ID: " + mapElementAPI.getKey());
		            	softasrt.assertEquals((mapElementSchema.getKey().toString()).equalsIgnoreCase(mapElementAPI.getKey().toString()), true, "JSON Doc ID: " + mapElementSchema.getKey() + " matched with API ID: " + mapElementAPI.getKey());
		            	//String dataType = mapSchema.get(mapElementSchema.getKey() + ".type").toString();
		            	String dataType1 = mapSchema.get(mapElementSchema.getKey()).toString();
		            	String[] datatypesplit = dataType1.split("type=")[1].split("}|,");
		            	String datatype = datatypesplit[0];
		            	
		            	System.out.println(datatype);
		            	
		            	String objType = mapElementAPI.getValue().getClass().getSimpleName();
		            	
		            	if(objType.equalsIgnoreCase(datatype)==false) {
		            	datatype = objType;
                    	}
		            	//}else if(datatype.equalsIgnoreCase("array")) {
//		            		datatype = "ArrayList";
//		            	}
		            	
	
		            	if(objType.equalsIgnoreCase(datatype)) {
		            		System.out.println("Correct Data type is displayed for the key: " + mapElementSchema.getKey());
		            		testLogger.log(Status.PASS,
									MarkupHelper.createLabel("Correct Data type is displayed for the key: " + mapElementSchema.getKey(), ExtentColor.GREEN));
		            		LoggingUtils.info("Correct Data type is displayed for the key: " + mapElementSchema.getKey());
		            		softasrt.assertEquals(objType.equalsIgnoreCase(datatype), true, "Correct Data type is displayed for the key: " + mapElementSchema.getKey());
							
		            	}else{
		            		System.out.println("Difference in data type for key: " + mapElementSchema.getKey() + " Schema Data Type is: " + datatype + " API Data Type is: " + objType);
		            		testLogger.log(Status.FAIL,
									MarkupHelper.createLabel("Difference in data type for key: " + mapElementSchema.getKey() + " Schema Data Type is: " + datatype + " API Data Type is: " + objType, ExtentColor.RED));
		            		LoggingUtils.error("Difference in data type for key: " + mapElementSchema.getKey() + " Schema Data Type is: " + datatype + " API Data Type is: " + objType);
		            		softasrt.assertEquals(false, true, "Difference in data type for key: " + mapElementSchema.getKey() + " Schema Data Type is: " + datatype + " API Data Type is: " + objType);
							}
	          	}else{
	          		System.out.println("Differences Found: JSON Doc Key is: " + mapElementSchema.getKey() + " and API Key is: " + mapElementAPI.getKey());
	          		testLogger.log(Status.FAIL,
							MarkupHelper.createLabel("Differences Found: JSON Doc Key is: " + mapElementSchema.getKey() + " and API Key is: " + mapElementAPI.getKey(), ExtentColor.RED));
	          		LoggingUtils.error("Differences Found: JSON Doc Key is: " + mapElementSchema.getKey() + " and API Key is: " + mapElementAPI.getKey());
	          		softasrt.assertEquals(false, true, "Differences Found: JSON Doc Key is: " + mapElementSchema.getKey() + " and API Key is: " + mapElementAPI.getKey());
					}
	          }
	     }
	}  
	
public Map<String, String> queryParamsArray(String jsonResource, List<String> queryValues) throws IOException {	
	 Response response=getJsonFromAPIDoc(jsonResource);
  JsonPath newData = response.jsonPath();
  String pathArray = newData.get("paths").toString();	    
  String [] newpath = pathArray.split("\\{", 2);
  String [] path = newpath[1].split("=");
  System.out.println(path[0]);
  String pathVar;
  if(path[0].contains("-")) {
	   pathVar = "\'" + path[0] + "\'";
  }else {
	   pathVar = "\"" + path[0] + "\"";
  }
  String callArray = newData.get("paths." + pathVar).toString();
  System.out.println(callArray);
  String [] newcall = callArray.split("\\{");
  String [] call = newcall[1].split("=");
  System.out.println(call[0]);
  Method type = Method.GET;
  if(call[0] == "get") {
 	 type = Method.GET;
  }else if (call[0] == "post"){
 	 type = Method.POST;
  }else if(call[0] == "put") {
	  	 type = Method.PUT;
	   }
  else if(call[0] == "delete") {
	  	 type = Method.DELETE;
	   }
  List <String> ResponseParamsArray = newData.getList("paths." + pathVar + "." + call[0] + ".parameters");

  System.out.println(ResponseParamsArray.size());
  HashMap<String, String> params = null;
  String ResponseArray2 = null;
  String ResponseArray3 = null;
  if(ResponseParamsArray.size() > 0) {
//	     System.out.println(ResponseParamsArray.size());
	     params = new HashMap<String, String>();
	     int j=0;
	     for(int i = 0; i < ResponseParamsArray.size(); i++) {
	    	 ResponseArray3 = newData.get("paths." + pathVar + "." + call[0] + ".parameters[" + i + "].in").toString();
	    	// System.out.println("In- ResponseArray3- "+ResponseArray3);
	    	 if(ResponseArray3.equalsIgnoreCase("query"))
	    	 {
	    		 ResponseArray2 = newData.get("paths." + pathVar + "." + call[0] + ".parameters[" + i + "].name").toString();
//		    	 System.out.println(ResponseArray2);
		    //	 String value = ResponseArray2;
		    	 params.put(ResponseArray2, queryValues.get(j)); 	
		    	 j++;
	    	 }
	    	     	 
	     } 
  }
	return params;
}




public Map<String, String> pathParamsArray(String jsonResource, List<String> queryValues) throws IOException {	
	 Response response=getJsonFromAPIDoc(jsonResource);
   JsonPath newData = response.jsonPath();
   String pathArray = newData.get("paths").toString();	    
   String [] newpath = pathArray.split("\\{", 2);
   String [] path = newpath[1].split("=");
//   System.out.println(path[0]);
   String pathVar;
   if(path[0].contains("-")) {
	   pathVar = "\'" + path[0] + "\'";
   }else {
	   pathVar = "\"" + path[0] + "\"";
   }
   String callArray = newData.get("paths." + pathVar).toString();
   String [] newcall = callArray.split("\\{");
   String [] call = newcall[1].split("=");
   System.out.println(call[0]);
   Method type = Method.GET;
   if(call[0] == "get") {
  	 type = Method.GET;
   }else if (call[0] == "post"){
  	 type = Method.POST;
   }else if(call[0] == "put") {
	  	 type = Method.PUT;
	   }
   else if(call[0] == "delete") {
	  	 type = Method.DELETE;
	   }
   List <String> ResponseParamsArray = newData.getList("paths." + pathVar + "." + call[0] + ".parameters");
//   System.out.println(ResponseParamsArray.size());
   HashMap<String, String> params = null;
   String ResponseArray2 = null;
   String ResponseArray3 = null;
   if(ResponseParamsArray.size() > 0) {
//	     System.out.println(ResponseParamsArray.size());
	     params = new HashMap<String, String>();
	     
	     for(int i = 0; i < ResponseParamsArray.size(); i++) {
	    	 ResponseArray3 = newData.get("paths." + pathVar + "." + call[0] + ".parameters[" + i + "].in").toString();
	    	 if(ResponseArray3.equalsIgnoreCase("path")) {
	    		 ResponseArray2 = newData.get("paths." + pathVar + "." + call[0] + ".parameters[" + i + "].name").toString();
//		    	 System.out.println(ResponseArray2);
		 //   	 String value = ResponseArray2;
		    	 params.put(ResponseArray2, queryValues.get(i));   
		    	
	    	 }	    	   	 
	     } 
   }
	return params;
}


public Map<String, Object> requestBodyArray(String jsonResource, List<String> bodyValues) throws IOException {	
	 Response response=getJsonFromAPIDoc(jsonResource);
  JsonPath newData = response.jsonPath();
  String pathArray = newData.get("paths").toString();	    
  String [] newpath = pathArray.split("\\{", 2);
  String [] path = newpath[1].split("=");
  System.out.println(path[0]);
  String pathVar;
  if(path[0].contains("-")) {
	   pathVar = "\'" + path[0] + "\'";
  }else {
	   pathVar = "\"" + path[0] + "\"";
  }
  String callArray = newData.get("paths." + pathVar).toString();
  String [] newcall = callArray.split("\\{");
  String [] call = newcall[1].split("=");
  System.out.println(call[0]);
  Method type = Method.GET;
  if(call[0] == "get") {
 	 type = Method.GET;
  }else if (call[0] == "post"){
 	 type = Method.POST;
  }else if(call[0] == "put") {
	  	 type = Method.PUT;
	   } else if(call[0] == "delete") {
	  	 type = Method.DELETE;
	   }
  Map<String, String> RequestBodyArray = newData.getMap("paths." + pathVar + "." + call[0] + ".requestBody");
  System.out.println(RequestBodyArray.size());
  Iterator hmIterator = RequestBodyArray.entrySet().iterator();
  String ResponseArray2 = null;
  Map<String,String> RequestBody = null;
  Map<Object, Object> RequestBodyValues = null;
  ArrayList<String> RequestBodyKeys = new ArrayList<String>();
  HashMap<String, Object> bodyKeyValuesMap=new HashMap<>();
  while (hmIterator.hasNext()) { 
 	 Map.Entry mapElement = (Map.Entry)hmIterator.next(); 
 	 //Map<String, String> map2 = (Map<String, String>) mapElement.getValue();
 	 System.out.println(mapElement.getKey());
 	 if(mapElement.getKey().equals("content")) {
		     RequestBody = new HashMap<String, String>();
		     String contentType = newData.get("paths." + pathVar + "." + call[0] + ".requestBody.content").toString();
		     System.out.println(contentType);
		     String [] contentTypePath = contentType.split("\\{", 2);
		     String [] contentTypePathValue = contentTypePath[1].split("=");
		     System.out.println(contentTypePathValue[0]);
		     
	    	 ResponseArray2 = newData.get("paths." + pathVar + "." + call[0] + ".requestBody.content['" + contentTypePathValue[0] + "'].schema.$ref").toString();
//	    	 System.out.println(ResponseArray2);
			 RequestSpecification httpRequest2 = RestAssured.given();
			 Response response2 = httpRequest2.request(Method.GET, ResponseArray2);
		     JsonPath newData2 = response2.jsonPath();
		     RequestBodyValues = newData2.getMap("components.schemas");
//		     System.out.println(RequestBodyValues.size());
		     Iterator hmBodyIterator = RequestBodyValues.entrySet().iterator(); 
		     while (hmBodyIterator.hasNext()) { 
		    	Map.Entry mapElementBody = (Map.Entry)hmBodyIterator.next();
		    	System.out.println(mapElementBody.getKey());
		    	String ele=mapElementBody.getKey().toString();
		    	if(ele.contains("Requestbody") || (ele.contains("RequestBody"))||(ele.contains("requestBody"))|| (ele.contains("reqBody")) || (ele.contains("ReqBody")) || (ele.contains("Request")) || (ele.contains("Req"))) {
		            Map<String, Map<String, ?>> map2 = (Map<String, Map<String, ?>>) mapElementBody.getValue();
//		            System.out.println(map2);
		            Map<String, ?> map3 = (Map<String, String>) map2.get("properties");
//		            System.out.println(map3.size());
		            Iterator hmBodyIteratorKeys = map3.entrySet().iterator();
		            
		            while (hmBodyIteratorKeys.hasNext()) { 
		            	Map.Entry mapElementBodyKeys = (Map.Entry)hmBodyIteratorKeys.next();
		            	System.out.println(mapElementBodyKeys.getKey());
		            	if(mapElementBodyKeys.getKey().equals("additional")) {
		            		Map<String, Map<String, ?>> map4 = (Map<String, Map<String, ?>>) mapElementBodyKeys.getValue();
//		            		System.out.println(map4);
		            		Map<String, ?> map5 = (Map<String, String>) map4.get("properties");
//		            		System.out.println(map5);
		            		Iterator additionalKeys = map5.entrySet().iterator();
		            		 while (additionalKeys.hasNext()) { 
		            			 Map.Entry additionalBodyKeys = (Map.Entry)additionalKeys.next();
		            			 RequestBodyKeys.add((String) additionalBodyKeys.getKey());
		            		 }
		            		 break;
		            	}else {
		            		RequestBodyKeys.add((String) mapElementBodyKeys.getKey());
		            	}
		            }
		            break;
		    	}

		     }
		     
 	 }
//	     System.out.println(bodyValues);
//	     System.out.println(RequestBodyKeys.size());
//	     System.out.println(bodyValues.size());
	     for(int i = 0; i < RequestBodyKeys.size(); i++) {
	    	 bodyKeyValuesMap.put(RequestBodyKeys.get(i), bodyValues.get(i));
	     }
  }
	return bodyKeyValuesMap;

}
	



public Map<String, String> bodyParamsArray(String jsonResource, List<String> queryValues) throws IOException {	
	 Response response=getJsonFromAPIDoc(jsonResource);
  JsonPath newData = response.jsonPath();
  String pathArray = newData.get("paths").toString();	    
  String [] newpath = pathArray.split("\\{", 2);
  String [] path = newpath[1].split("=");
  System.out.println(path[0]);
  String pathVar;
  if(path[0].contains("-")) {
	   pathVar = "\'" + path[0] + "\'";
  }else {
	   pathVar = "\"" + path[0] + "\"";
  }
  String callArray = newData.get("paths." + pathVar).toString();
  String [] newcall = callArray.split("\\{");
  String [] call = newcall[1].split("=");
  System.out.println(call[0]);
  Method type = Method.GET;
  if(call[0] == "get") {
 	 type = Method.GET;
  }else if (call[0] == "post"){
 	 type = Method.POST;
  }else if(call[0] == "put") {
	  	 type = Method.PUT;
	   }
  else if(call[0] == "delete") {
	  	 type = Method.DELETE;
	   }
  List <String> ResponseParamsArray = newData.getList("paths." + pathVar + "." + call[0] + ".parameters");
//  System.out.println(ResponseParamsArray.size());
  HashMap<String, String> params = null;
  String ResponseArray2 = null;
  String ResponseArray3 = null;
  if(ResponseParamsArray.size() > 0) {
//	     System.out.println(ResponseParamsArray.size());
	     params = new HashMap<String, String>();
	     
	     for(int i = 0; i < ResponseParamsArray.size(); i++) {
	    	 ResponseArray3 = newData.get("paths." + pathVar + "." + call[0] + ".parameters[" + i + "].in").toString();
	    	 if(ResponseArray3.equalsIgnoreCase("body")) {
	    		 ResponseArray2 = newData.get("paths." + pathVar + "." + call[0] + ".parameters[" + i + "].name").toString();
//		    	 System.out.println(ResponseArray2);
		 //   	 String value = ResponseArray2;
		    	 params.put(ResponseArray2, queryValues.get(i));   
		    	
	    	 }	    	   	 
	     } 
  }
	return params;
}



@BeforeSuite
	public void setUp() {
		String extReportPath;
		if(os.equalsIgnoreCase("windows")){
			extReportPath = path + "\\Reports\\extent-report.html";
		}else{
				extReportPath = path + "//Reports//extent-report.html";

			}

	    htmlReporter = new ExtentHtmlReporter(extReportPath);
	    extent = new ExtentReports();
	    extent.attachReporter(htmlReporter);

	    extent.setSystemInfo("Report Name","ReST API Report");

	    htmlReporter.config().setDocumentTitle("AutomationTesting in ReST API");
	    htmlReporter.config().setReportName("ReST API Report");
	}
	

	@AfterMethod
	public void getResult(ITestResult result){
	    if(result.getStatus()==ITestResult.FAILURE){
	        testLogger.fail(MarkupHelper.createLabel(result.getName()+" Test Case is FAILED", ExtentColor.RED));
	        testLogger.fail(result.getThrowable());
	    }
	    else if(result.getStatus()==ITestResult.SUCCESS){
	    	testLogger.pass(MarkupHelper.createLabel(result.getName()+" Test Case is PASSED",ExtentColor.GREEN));
	    }
	    else {
	    	testLogger.skip(MarkupHelper.createLabel(result.getName()+" Test Case is SKIPPED",ExtentColor.YELLOW));
	    	testLogger.skip(result.getThrowable());
	    }
	}
	
	@AfterTest
	public void asserterrors() {
		//softasrt.assertAll();
	}

	@AfterSuite
	public void tearDown(){
	    extent.flush();
	}
	
}