package com.ott.RequestApiEngine;

public class PostWatchHistoryReq {

	public String id;
	public String assetType;
	public String duration;

	public PostWatchHistoryReq(String id, String assetType, String duration) {
	this.id = id;
	this.assetType = assetType;
	this.duration = duration;
	}
	}
