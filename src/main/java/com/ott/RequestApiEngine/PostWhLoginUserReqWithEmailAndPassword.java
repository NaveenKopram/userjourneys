package com.ott.RequestApiEngine;

public class PostWhLoginUserReqWithEmailAndPassword {
	public String email;
	public String password;
	public String guest_token;
	public String platform;
	public String device;
	public String aid;
	public String lotame_cookie_id;
	public String vesion;
	
	public PostWhLoginUserReqWithEmailAndPassword(String email, String password, String guest_token, String platform,
			String device, String aid, String lotame_cookie_id, String vesion) {
		super();
		this.email = email;
		this.password = password;
		this.guest_token = guest_token;
		this.platform = platform;
		this.device = device;
		this.aid = aid;
		this.lotame_cookie_id = lotame_cookie_id;
		this.vesion = vesion;
	}
	

}
