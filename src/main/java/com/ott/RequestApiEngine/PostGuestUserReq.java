package com.ott.RequestApiEngine;

public class PostGuestUserReq {

public String aid;
public String apikey;
public String dekeyVal;
public String UserAgent;

/**
*
* @param apikey
* @param dekeyVal
* @param userAgent
* @param aid
*/
public PostGuestUserReq(String aid, String apikey, String dekeyVal, String UserAgent) {
this.aid = aid;
this.apikey = apikey;
this.dekeyVal = dekeyVal;
this.UserAgent = UserAgent;
}

}