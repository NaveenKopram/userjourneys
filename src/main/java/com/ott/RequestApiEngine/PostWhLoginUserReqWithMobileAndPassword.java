package com.ott.RequestApiEngine;

public class PostWhLoginUserReqWithMobileAndPassword {
	public String mobile;
	public String password;
	public String guest_token;
	public String platform;
	public String device;
	public String aid;
	public String lotame_cookie_id;
	public String vesion;
	public PostWhLoginUserReqWithMobileAndPassword(String mobile, String password, String guest_token,
			String platform, String device, String aid, String lotame_cookie_id, String vesion) {
		this.mobile = mobile;
		this.password = password;
		this.guest_token = guest_token;
		this.platform = platform;
		this.device = device;
		this.aid = aid;
		this.lotame_cookie_id = lotame_cookie_id;
		this.vesion = vesion;
	}
	

}
