package resources;

import java.io.IOException;

import org.testng.annotations.DataProvider;

import commonFunctions.TestUtil;
import utils.Excel;

public class UserJourney02_TestData_DataProvider extends TestUtil {
	public static String excelPath() {
		String filePath;
		if(os.equalsIgnoreCase("windows")){
			filePath = path + "\\src\\main\\java\\resources\\UserJourney_02_TestData_" + env + ".xlsx";
		}else{
			filePath = path + "//src//main//java//resources//UserJourney_02_TestData_" + env + ".xlsx";

			}
		return filePath;
	}
	@DataProvider(name = "LaunchAPI_CountryList")
    public static Object[][] LaunchAPI_CountryList() throws IOException{
		return Excel.getTestData(excelPath(), "LaunchAPI_CountryList", "LaunchAPI_CountryList");			
	} 
	@DataProvider(name = "b2bapi_getlang")
    public static Object[][] b2bapi_getlang() throws IOException{
		return Excel.getTestData(excelPath(), "getlang", "b2bapi_getlang");			
	} 
	@DataProvider(name = "launchapi_Launch")
    public static Object[][] launchapi_Launch() throws IOException{
		return Excel.getTestData(excelPath(), "launchapi_Launch", "launchapi_Launch");			
	} 
	@DataProvider(name = "launchapi_PlatformTokens")
    public static Object[][] launchapi_PlatformTokens() throws IOException{
		return Excel.getTestData(excelPath(), "launchapi_PlatformTokens", "launchapi_PlatformTokens");			
	} 
	
	@DataProvider(name = "whapi_whuser_loginemail")
    public static Object[][] whapi_whuser_loginemail() throws IOException{
		return Excel.getTestData(excelPath(), "whuser_loginemail", "whapi_whuser_loginemail");			
	} 
	@DataProvider(name = "userapi_settings_getsettings")
    public static Object[][] userapi_settings_getsettings() throws IOException{
		return Excel.getTestData(excelPath(), "settings_getsettings", "userapi_settings_getsettings");			
	} 
	
	@DataProvider(name = "gwapi_content_getcontent_search")
    public static Object[][] gwapi_content_getcontent_search() throws IOException{
		return Excel.getTestData(excelPath(), "content_getcontent_search", "gwapi_content_getcontent_search");			
	} 
	@DataProvider(name = "gwapi_content_collectionapi")
    public static Object[][] gwapi_content_collectionapi() throws IOException{
		return Excel.getTestData(excelPath(), "content_collectionapi", "gwapi_content_collectionapi");			
	} 
	
	@DataProvider(name = "singleplaybackapi_singleplaybackgetdetails")
    public static Object[][] singleplaybackapi_singleplaybackgetdetails() throws IOException{
		return Excel.getTestData(excelPath(), "singleplaybackgetdetails", "singleplaybackapi_singleplaybackgetdetails");			
	} 
	@DataProvider(name = "subscription_Plan_GetSubscriptionPlans")
    public static Object[][] subscription_Plan_GetSubscriptionPlans() throws IOException{
		return Excel.getTestData(excelPath(), "Plan_GetSubscriptionPlans", "subscription_Plan_GetSubscriptionPlans");			
	} 

}
