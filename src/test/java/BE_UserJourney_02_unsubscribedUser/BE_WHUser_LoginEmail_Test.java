package BE_UserJourney_02_unsubscribedUser;

import static io.restassured.RestAssured.given;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

import commonFunctions.TestUtil;
import io.restassured.specification.FilterableRequestSpecification;

import resources.UserJourney02_TestData_DataProvider;

import utils.LoggingUtils;

public class BE_WHUser_LoginEmail_Test extends TestUtil
{

//	RequestSpecification req;
//	Response response;
	SoftAssert softasrt; 
	


       @Test(dataProviderClass = UserJourney02_TestData_DataProvider.class, dataProvider = "whapi_whuser_loginemail", description="Login Email", enabled = true)
       public void whapi_whuser_loginemail(String responseCode, String schemaName, String errorCode, String message, String email, String password, String guest_token, String platform, String device, String aid, String lotame_cookie_id, String version) throws IOException {
	
	
	    testLogger=extent.createTest("whapi_whuser_loginemail :"+responseCode,"Test to Validate Response-"+responseCode +"</br>Test Data: </br>responseCode- "+responseCode+"</br>schemaName- "+schemaName+"</br>errorCode- "+errorCode+"</br>message- "+message+"</br>email- "+email+"</br>password- "+password+"</br>guest_token- "+guest_token+"</br>platform- "+platform+"</br>device- "+device+"</br>aid- "+aid+"</br>lotame_cookie_id- "+lotame_cookie_id+"</br>version- "+version);		
		testLogger.info("Call post whapi whuser loginemail: whapi_whuser_loginemail</br>Endpoint url-"+getGlobalValue(BaseUrl_whAPI)+getResource("whapi_whuser_loginemail_path"));
		LoggingUtils.info("Call post whapi whuser loginemail: whapi_whuser_loginemail</br>Endpoint url-"+getGlobalValue(BaseUrl_whAPI)+getResource("whapi_whuser_loginemail_path"));
		softasrt=new SoftAssert(); 
		

		List<String> bodyValues = new ArrayList();
	    List<String> list = Arrays.asList(email,password,guest_token,platform,device,aid,lotame_cookie_id,version);
	    bodyValues.addAll(list);
	    System.out.println(bodyValues);
	    Map<String, Object> requestBodyArray = (Map<String, Object>) requestBodyArray("whapi_whuser_loginemailJson",bodyValues);
	    System.out.println(requestBodyArray);	
  		
			req = (FilterableRequestSpecification) given().spec(requestSpecification(BaseUrl_whAPI))
					.body(requestBodyArray);
		response = req.when().post(getResource("whapi_whuser_loginemail_path")).then().extract().response();
		boolean status=false;
		String actStatusCode=Integer.toString(response.getStatusCode());
		 System.out.println(actStatusCode);
		 if((responseCode.contains(actStatusCode))) {
				if((actStatusCode.contains("400")&& response.asString().contains(errorCode) && response.asString().contains(message))||(actStatusCode.contains("401")&& response.asString().contains(errorCode) && response.asString().contains(message))) {
					 testLogger.log(Status.PASS,
								MarkupHelper.createLabel("Response Time(ms)is-"+response.getTimeIn(TimeUnit.MILLISECONDS), ExtentColor.GREEN));
					 LoggingUtils.info("Response Time(ms)is-"+response.getTimeIn(TimeUnit.MILLISECONDS));
					 testLogger.log(Status.PASS,
								MarkupHelper.createLabel("Expected Response code is returned-"+actStatusCode, ExtentColor.GREEN));
					 LoggingUtils.info("Expected Response code is returned-"+actStatusCode);
					 Map<String, String> mapAPI = convertAPIResponseToHashMap(response);
					compareJsonSchemaAndResponseBody(mapAPI, "whapi_whuser_loginemailJson", responseCode, "whapi_whuser_loginemail_path", softasrt);		    
					status=true;	
				}else if(actStatusCode.contains("200")) {
				 
				 testLogger.log(Status.PASS,
							MarkupHelper.createLabel("Response Time(ms)is-"+response.getTimeIn(TimeUnit.MILLISECONDS), ExtentColor.GREEN));
				 LoggingUtils.info("Response Time(ms)is-"+response.getTimeIn(TimeUnit.MILLISECONDS));
				 testLogger.log(Status.PASS,
							MarkupHelper.createLabel("Expected Response code is returned-"+actStatusCode, ExtentColor.GREEN));
				 LoggingUtils.info("Expected Response code is returned-"+actStatusCode);
				 Map<String, String> mapAPI = convertAPIResponseToHashMap(response);
				compareJsonSchemaAndResponseBody(mapAPI, "whapi_whuser_loginemailJson", responseCode, "whapi_whuser_loginemail_path", softasrt);		    
				status=true;
				}
			 }
		 if(status) {
				softasrt.assertEquals(status, true, "whapi_whuser_loginemail - PASSED");
				System.out.println("whapi_whuser_loginemail - PASSED");
				testLogger.log(Status.PASS,
						MarkupHelper.createLabel("whapi_whuser_loginemail: Expected Response code is returned and schema is mapping proerly.", ExtentColor.GREEN));
				LoggingUtils.info("whapi_whuser_loginemail: Expected Response code is returned and schema is mapping proerly.");
				softasrt.assertAll();
				}else {
				System.out.println("whapi_whuser_loginemail - FAILED");
				testLogger.log(Status.FAIL,
						MarkupHelper.createLabel("whapi_whuser_loginemail - FAILED", ExtentColor.RED));
				LoggingUtils.error("whapi_whuser_loginemail - FAILED");
				softasrt.assertEquals(false, true, "whapi_whuser_loginemail - FAILED");					
				softasrt.assertAll();
				}
  }
  
  
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
