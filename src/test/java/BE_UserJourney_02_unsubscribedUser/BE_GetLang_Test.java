package BE_UserJourney_02_unsubscribedUser;

import static io.restassured.RestAssured.given;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

import commonFunctions.TestUtil;
import io.restassured.specification.FilterableRequestSpecification;

import resources.UserJourney02_TestData_DataProvider;
import utils.Json;
import utils.LoggingUtils;

public class BE_GetLang_Test extends TestUtil
{

	SoftAssert softasrt;
	
	 @Test(dataProviderClass = UserJourney02_TestData_DataProvider.class, dataProvider = "b2bapi_getlang", description="Get Lang", enabled = true)
	  public void b2bapi_getlang(String responseCode, String schemaName, String errorCode, String message, String lang_localized_order, String country, String state, String translation, String platform_name, String version,String jira_id) throws IOException, InterruptedException {
		   
		 testLogger=extent.createTest("b2bapi_getlang :"+responseCode,"Test to Validate Response-"+responseCode+"</br>Test Data: </br>responseCode- "+responseCode+"</br>schemaName- "+schemaName+"</br>errorCode- "+errorCode+"</br>message- "+message+"</br>lang_localized_order- "+lang_localized_order+"</br>state- "+country+"</br>path-" +state+ "</br>translation-"+translation+"</br>platform_name-"+platform_name+"</br>version-"+version);	
			testLogger.info("Call post b2bapi getlang: b2bapi_getlang</br>Endpoint url-"+getGlobalValue(BaseUrl_b2bAPI)+getResource("b2bapi_getlang_path"));
			LoggingUtils.info("Call post b2bapi getlang: b2bapi_getlang</br>Endpoint url-"+getGlobalValue(BaseUrl_b2bAPI)+getResource("b2bapi_getlang_path"));
			softasrt=new SoftAssert();
			
			 List<String> queryValues = new ArrayList();
			 List<String> list = Arrays.asList(lang_localized_order,country,state,translation,platform_name,version);
		     queryValues.addAll(list);		    
		     HashMap<String, String> params = (HashMap<String, String>) queryParamsArray("b2bapi_getlangJson", queryValues);
		     System.out.println(params);
		     
		    req = (FilterableRequestSpecification) given().spec(requestSpecification(BaseUrl_b2bAPI)).queryParams(params);
			
			response = req.when().post(getResource("b2bapi_getlang_path")).then().extract().response();
			boolean status=false;
			String actStatusCode=Integer.toString(response.getStatusCode());
			 System.out.println(actStatusCode);
			 if((responseCode.contains(actStatusCode))) {
					if((actStatusCode.contains("404")&& response.asString().contains(errorCode) && response.asString().contains(message))||(actStatusCode.contains("400")&& response.asString().contains(errorCode) && response.asString().contains(message))||(actStatusCode.contains("401")&& response.asString().contains(errorCode) && response.asString().contains(message))) {
						 testLogger.log(Status.PASS,
									MarkupHelper.createLabel("Response Time(ms)is-"+response.getTimeIn(TimeUnit.MILLISECONDS), ExtentColor.GREEN));
						 LoggingUtils.info("Response Time(ms)is-"+response.getTimeIn(TimeUnit.MILLISECONDS));
						 testLogger.log(Status.PASS,
									MarkupHelper.createLabel("Expected Response code is returned-"+actStatusCode, ExtentColor.GREEN));
						 LoggingUtils.info("Expected Response code is returned-"+actStatusCode);
						 Map<String, String> mapAPI = convertAPIResponseToHashMap(response);
						compareJsonSchemaAndResponseBody(mapAPI, "b2bapi_getlangJson", responseCode, "b2bapi_getlang_path", softasrt);		    
						status=true;	
					}else if(actStatusCode.contains("200")) {
					 
					 testLogger.log(Status.PASS,
								MarkupHelper.createLabel("Response Time(ms)is-"+response.getTimeIn(TimeUnit.MILLISECONDS), ExtentColor.GREEN));
					 LoggingUtils.info("Response Time(ms)is-"+response.getTimeIn(TimeUnit.MILLISECONDS));
					 testLogger.log(Status.PASS,
								MarkupHelper.createLabel("Expected Response code is returned-"+actStatusCode, ExtentColor.GREEN));
					 LoggingUtils.info("Expected Response code is returned-"+actStatusCode);
					 Map<String, String> mapAPI = convertAPIResponseToHashMap(response);
					compareJsonSchemaAndResponseBody(mapAPI, "b2bapi_getlangJson", responseCode, "b2bapi_getlang_path", softasrt);		    
					status=true;
					}
				 }
			 if(status) {
				// softasrt.assertEquals(status, true, "b2bapi_getlang - PASSED");
					System.out.println("b2bapi_getlang - PASSED");
					testLogger.log(Status.PASS,
							MarkupHelper.createLabel("b2bapi_getlang: Expected Response code is returned and schema is mapping proerly.", ExtentColor.GREEN));
					LoggingUtils.info("b2bapi_getlang: Expected Response code is returned and schema is mapping proerly.");
					softasrt.assertAll();
					}else {
						System.out.println("b2bapi_getlang - FAILED");
					testLogger.log(Status.FAIL,
							MarkupHelper.createLabel("b2bapi_getlang - FAILED", ExtentColor.RED));
					LoggingUtils.error("b2bapi_getlang - FAILED");
					softasrt.assertEquals(false, true, "b2bapi_getlang - FAILED");					
					softasrt.assertAll();
					}
		  
	  }
	
	 
	
	
	
	
	
	
	
	
	
	
	
}
