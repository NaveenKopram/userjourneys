package BE_UserJourney_02_unsubscribedUser;

import static io.restassured.RestAssured.given;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

import commonFunctions.TestUtil;
import io.restassured.specification.FilterableRequestSpecification;

import resources.UserJourney02_TestData_DataProvider;
import utils.LoggingUtils;

public class BE_SinglePlaybackGetDetails_Test extends TestUtil
{
	SoftAssert softasrt;
	
	
	 @Test(dataProviderClass = UserJourney02_TestData_DataProvider.class, dataProvider = "singleplaybackapi_singleplaybackgetdetails", description="Single Playback Get Details", enabled = true)
	  public void singleplaybackapi_singleplaybackgetdetails(String responseCode, String schemaName, String errorCode, String message,String username,String password,
			  String data_source, String content_id ,String channel_id,
			  String device_id ,String session_id,String check_parental_control,
			  String current_parental_control, String platform_name, String country ,
			  String is_latest, String is_marketing, String state,
			  String html5, String vms,String streamType,
			  String show_id, String translation, String user_language, 
			  String collection_id, String app_version, String is_lat,
			  String bd_source, String dishtv_free_trial, String free_trial,
			  String utm_source,String utm_medium,String utm_campaign,
			  String utm_term,String utm_content,String utmpageurl_latest,
			  String dekey,String display_language,String gender,
			  String age,String ads_visibility,String brand,
			  String model ,String kid,String isSb,String version) throws IOException {
		    testLogger=extent.createTest("singleplaybackapi_singleplaybackgetdetails :"+responseCode,"Test to Validate Response-"+responseCode+"</br>Test Data: </br>responseCode- "+responseCode+"</br>schemaName- "+schemaName+"</br>errorCode- "+errorCode+"</br>message- "+message+"</br>content_id - "+content_id +"</br>device_id - "+device_id +"</br>platform_name-"+platform_name+"</br>country - "+country +"</br>is_latest- "+is_latest+"</br>is_marketing- "+is_marketing+"</br>state- "+state+"</br>vms- "+vms+"</br>translation- "+translation+"</br>user_language- "+user_language+"</br>collection_id- "+collection_id+"</br>app_version- "+app_version+"</br>is_lat- "+is_lat+"</br>bd_source - "+bd_source +"</br>dishtv_free_trial-"+dishtv_free_trial+"</br>free_trial - "+free_trial +"</br>utm_source- "+utm_source+"</br>utm_medium- "+utm_medium+"</br>utm_campaign- "+utm_campaign+"</br>utm_term- "+utm_term+"</br>utm_content- "+utm_content+"</br>utmpageurl_latest- "+utmpageurl_latest+"</br>dekey- "+dekey+"</br>display_language- "+display_language+"</br>gender- "+gender+"</br>age- "+age);		
			testLogger.info("Call post singleplayback api singleplayback getdetails API: singleplaybackapi_singleplaybackgetdetails</br>Endpoint url-"+getGlobalValue(BaseUrl_singlePlaybackAPI)+getResource("singleplaybackapi_singleplaybackgetdetails_path"));
			LoggingUtils.info("Call post singleplayback api singleplayback getdetails API: singleplaybackapi_singleplaybackgetdetails</br>Endpoint url-"+getGlobalValue(BaseUrl_singlePlaybackAPI)+getResource("singleplaybackapi_singleplaybackgetdetails_path"));
			softasrt=new SoftAssert();
			
			 List<String> queryValues = new ArrayList();
			 List<String> list = Arrays.asList(data_source,content_id,channel_id, device_id ,session_id,check_parental_control,current_parental_control, platform_name, country , is_latest, is_marketing, state,html5,streamType,show_id, vms, translation, user_language,collection_id,app_version,is_lat,bd_source,dishtv_free_trial,free_trial,utm_source,utm_medium,utm_campaign,utm_term,utm_content,utmpageurl_latest,dekey,display_language,gender,age,ads_visibility,brand,model,kid,isSb,version);
		     queryValues.addAll(list);		    
		     HashMap<String, String> params = (HashMap<String, String>) queryParamsArray("singleplaybackapi_singleplaybackgetdetailsJson", queryValues);
		     System.out.println(params);
		     
		    
		  
		     String Authorization = getAuthorizationForEmailLogin(username, password);
		     
		     String xaccesstoken = "eyJ0eXAiOiJqd3QiLCJhbGciOiJIUzI1NiJ9.eyJwcm9kdWN0X2NvZGUiOiJ6ZWU1QDk3NSIsInBsYXRmb3JtX2NvZGUiOiJsZ0B0dkBhcHBsaWNhdGlvbiIsImlzc3VlZEF0IjoiMjAyMS0wOC0zMVQwNjozMDowMiswMDAwIiwidHRsIjo4NjQwMH0.qyyt7AiyT3NiWwjTD2AU6QvXz67GdRGOM7ticH_HjE8";
		     
				/*
				 * List<String> bodyValues = new ArrayList(); List<String> list1 =
				 * Arrays.asList(Authorization); bodyValues.addAll(list);
				 * System.out.println(bodyValues); Map<String, Object> requestBodyArray =
				 * (Map<String, Object>)
				 * requestBodyArray("singleplaybackapi_singleplaybackgetdetailsJson",bodyValues)
				 * ; System.out.println(requestBodyArray);
				 */
		     Map<String, Object>  bodyJsonAsMap = new HashMap<>();
		     bodyJsonAsMap.put("Authorization", Authorization);
		     bodyJsonAsMap.put("x-access-token", xaccesstoken);
			    
		     
				req = (FilterableRequestSpecification) given().spec(requestSpecification(BaseUrl_singlePlaybackAPI))
						.queryParams(params).body(bodyJsonAsMap);
			
			response = req.when().post(getResource("singleplaybackapi_singleplaybackgetdetails_path")).then().extract().response();
			boolean status=false;
			String actStatusCode=Integer.toString(response.getStatusCode());
			 System.out.println(actStatusCode);
			 if((responseCode.contains(actStatusCode))) {
					if((actStatusCode.contains("400")&& response.asString().contains(errorCode) && response.asString().contains(message))||(actStatusCode.contains("401")&& response.asString().contains(errorCode) && response.asString().contains(message))) {
						 testLogger.log(Status.PASS,
									MarkupHelper.createLabel("Response Time(ms)is-"+response.getTimeIn(TimeUnit.MILLISECONDS), ExtentColor.GREEN));
						 LoggingUtils.info("Response Time(ms)is-"+response.getTimeIn(TimeUnit.MILLISECONDS));
						 testLogger.log(Status.PASS,
									MarkupHelper.createLabel("Expected Response code is returned-"+actStatusCode, ExtentColor.GREEN));
						 LoggingUtils.info("Expected Response code is returned-"+actStatusCode);
						 Map<String, String> mapAPI = convertAPIResponseToHashMap(response);
					//	compareJsonSchemaAndResponseBody(mapAPI, "singleplaybackapi_singleplaybackgetdetailsJson", responseCode, "singleplaybackapi_singleplaybackgetdetails_path", softasrt);		    
						status=true;	
					}else if(actStatusCode.contains("200")) {
					 
					 testLogger.log(Status.PASS,
								MarkupHelper.createLabel("Response Time(ms)is-"+response.getTimeIn(TimeUnit.MILLISECONDS), ExtentColor.GREEN));
					 LoggingUtils.info("Response Time(ms)is-"+response.getTimeIn(TimeUnit.MILLISECONDS));
					 testLogger.log(Status.PASS,
								MarkupHelper.createLabel("Expected Response code is returned-"+actStatusCode, ExtentColor.GREEN));
					 LoggingUtils.info("Expected Response code is returned-"+actStatusCode);
					 Map<String, String> mapAPI = convertAPIResponseToHashMap(response);
					compareJsonSchemaAndResponseBody(mapAPI, "singleplaybackapi_singleplaybackgetdetailsJson", responseCode, "singleplaybackapi_singleplaybackgetdetails_path", softasrt);		    
					status=true;
					}
				 }
			 if(status) {
					softasrt.assertEquals(status, true, "singleplaybackapi_singleplaybackgetdetails - PASSED");
					System.out.println("singleplaybackapi_singleplaybackgetdetails - PASSED");
					testLogger.log(Status.PASS,
							MarkupHelper.createLabel("singleplaybackapi_singleplaybackgetdetails: Expected Response code is returned and schema is mapping proerly.", ExtentColor.GREEN));
					LoggingUtils.info("singleplaybackapi_singleplaybackgetdetails: Expected Response code is returned and schema is mapping proerly.");
					softasrt.assertAll();
					}else {
					System.out.println("singleplaybackapi_singleplaybackgetdetails - FAILED");
					testLogger.log(Status.FAIL,
							MarkupHelper.createLabel("singleplaybackapi_singleplaybackgetdetails - FAILED", ExtentColor.RED));
					LoggingUtils.error("singleplaybackapi_singleplaybackgetdetails - FAILED");
					softasrt.assertEquals(false, true, "singleplaybackapi_singleplaybackgetdetails - FAILED");					
					softasrt.assertAll();
					}
	  }
	  






	
	
}
