package BE_UserJourney_02_unsubscribedUser;

import static io.restassured.RestAssured.given;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

import commonFunctions.TestUtil;
import io.restassured.response.Response;
import io.restassured.specification.FilterableRequestSpecification;
import io.restassured.specification.RequestSpecification;

import resources.UserJourney02_TestData_DataProvider;
import utils.LoggingUtils;

public class BE_Settings_GetSettings_Test extends TestUtil
{
//	RequestSpecification req;
//	Response response;
	SoftAssert softasrt;
	String date=new SimpleDateFormat("dd/MM/yy").format(Calendar.getInstance().getTime());
	
	
	
	@Test(dataProviderClass = UserJourney02_TestData_DataProvider.class, dataProvider = "userapi_settings_getsettings", description="Get Settings", enabled = true)
	  public void userapi_settings_getsettings(String responseCode,String schemaName,String errorCode,String message, String username, String password)
			  throws IOException {
		    testLogger=extent.createTest("userapi_settings_getsettings :"+responseCode,"Test to Validate Response-"+responseCode +"</br>Test Data: </br>responseCode- "+responseCode+"</br>schemaName- "+schemaName+"</br>code- "+errorCode+"</br>message- "+message+"</br>username- "+username+"</br>password- "+password);		
			testLogger.info("Call post userapi settings getsettings API: userapi_settings_getsettings</br>Endpoint url-"+getGlobalValue(BaseUrl_userAPI)+getResource("userapi_settings_getsettings_path"));
			LoggingUtils.info("Call post userapi settings getsettings API: userapi_settings_getsettings</br>Endpoint url-"+getGlobalValue(BaseUrl_userAPI)+getResource("userapi_settings_getsettings_path"));
			softasrt=new SoftAssert();
			
			if (responseCode.contains("401")) {
				req = (FilterableRequestSpecification) given().spec(requestSpecification(BaseUrl_userAPI));
						
			} else {
				//get Authorization
				String authorization = getAuthorizationForEmailLogin(username, password);
				
				req = (FilterableRequestSpecification) given().spec(requestSpecification(BaseUrl_userAPI))
						.header("Authorization", authorization);
			}
			response = req.when().get(getResource("userapi_settings_getsettings_path")).then().extract().response();
			boolean status=false;
			 String actStatusCode=Integer.toString(response.getStatusCode());
			 System.out.println(actStatusCode);
			 if((responseCode.contains(actStatusCode))) {
					if((actStatusCode.contains("400")&& response.asString().contains(errorCode) && response.asString().contains(message))||(actStatusCode.contains("404")&& response.asString().contains(errorCode) && response.asString().contains(message))||(actStatusCode.contains("401")&& response.asString().contains(errorCode) && response.asString().contains(message))) {
						 testLogger.log(Status.PASS,
									MarkupHelper.createLabel("Response Time(ms)is-"+response.getTimeIn(TimeUnit.MILLISECONDS), ExtentColor.GREEN));
						 LoggingUtils.info("Response Time(ms)is-"+response.getTimeIn(TimeUnit.MILLISECONDS));
						 testLogger.log(Status.PASS,
									MarkupHelper.createLabel("Expected Response code is returned-"+actStatusCode, ExtentColor.GREEN));
						 LoggingUtils.info("Expected Response code is returned-"+actStatusCode);
		 				 Map<String, String> mapAPI = convertAPIResponseToHashMap(response);
						compareJsonSchemaAndResponseBody(mapAPI, "userapi_settings_getsettingsJson", responseCode, "userapi_settings_getsettings_path", softasrt);		    
						status=true;	
					}else if(actStatusCode.contains("200")) {
					 
					 testLogger.log(Status.PASS,
								MarkupHelper.createLabel("Response Time(ms)is-"+response.getTimeIn(TimeUnit.MILLISECONDS), ExtentColor.GREEN));
					 LoggingUtils.info("Response Time(ms)is-"+response.getTimeIn(TimeUnit.MILLISECONDS));
					 testLogger.log(Status.PASS,
								MarkupHelper.createLabel("Expected Response code is returned-"+actStatusCode, ExtentColor.GREEN));
					 LoggingUtils.info("Expected Response code is returned-"+actStatusCode);
				//	 Map<String, String> mapAPI = convertArrayAPIResponseToHashMap(response);
					 Map<String, String> mapAPI = convertListAPIResponseToHashMap(response); 
					 compareJsonSchemaAndResponseBodyArray(mapAPI, "userapi_settings_getsettingsJson", responseCode, "userapi_settings_getsettings_path", softasrt);		    
					status=true;
					}
				 }
			 if(status) {
					softasrt.assertEquals(status, true, "userapi_settings_getsettings - PASSED");
					System.out.println("userapi_settings_getsettings - PASSED");
					testLogger.log(Status.PASS,
							MarkupHelper.createLabel("userapi_settings_getsettings: Expected Response code is returned and schema is mapping proerly.", ExtentColor.GREEN));
					LoggingUtils.info("userapi_settings_getsettings: Expected Response code is returned and schema is mapping proerly.");
					softasrt.assertAll();
					}else {
					System.out.println("userapi_settings_getsettings - FAILED");
					testLogger.log(Status.FAIL,
							MarkupHelper.createLabel("userapi_settings_getsettings - FAILED", ExtentColor.RED));
					LoggingUtils.error("userapi_settings_getsettings - FAILED");
					softasrt.assertEquals(false, true, "userapi_settings_getsettings - FAILED");					
					softasrt.assertAll();
					}
	  }
	  

	
	
	

}
