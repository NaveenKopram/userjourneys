package BE_UserJourney_02_unsubscribedUser;

import static io.restassured.RestAssured.given;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

import commonFunctions.TestUtil;
import io.restassured.specification.FilterableRequestSpecification;

import resources.UserJourney02_TestData_DataProvider;
import utils.Json;
import utils.LoggingUtils;

public class BE_Content_CollectionAPI_Test extends TestUtil{

	SoftAssert softasrt;
	
	
	 @Test(dataProviderClass = UserJourney02_TestData_DataProvider.class, dataProvider = "gwapi_content_collectionapi",description = "Collection API" ,enabled = true)
	  public void gwapi_content_collectionapi(String responseCode, String schemaName, String errorCode, String message,String username ,String password,String xAccessToken,String asset_id, String translation, String page, String limit,String item_limit,String country, String languages,String epg_data,String jira_id) throws IOException, InterruptedException {
//		 try { 
		 testLogger=extent.createTest("gwapi_content_collectionapi :"+responseCode,"Test to Validate Response-"+responseCode+"</br>Test Data: </br>responseCode- "+responseCode+"</br>schemaName- "+schemaName+"</br>errorCode- "+errorCode+"</br>message- "+message+"</br>xAccessToken -"+xAccessToken+"</br>username -"+username+"</br>password- "+password+"</br>asset_id-"+asset_id+"</br>translation- "+translation+"</br>page- "+page+"</br>limit -"+limit+"</br>item_limit - "+item_limit+"</br>country - "+country+"</br>languages -"+languages+"</br>epg_data- "+epg_data);		
			testLogger.info("Call Get gwapi Content collectionapi: gwapi_content_collectionapi</br>Endpoint url-"+getGlobalValue(BaseUrl_gwAPI)+getResource("gwapi_content_collectionapi_path"));
			LoggingUtils.info("Call Get gwapi Content collectionapi: gwapi_content_collectionapi</br>Endpoint url-"+getGlobalValue(BaseUrl_gwAPI)+getResource("gwapi_content_collectionapi_path"));
			softasrt=new SoftAssert();
			
			 List<String> pathValues = new ArrayList();
	         pathValues.add(asset_id);         
	         HashMap<String, String> params1 = (HashMap<String, String>) pathParamsArray("gwapi_content_collectionapiJson", pathValues);
	         System.out.println(params1);
			
			
			 List<String> queryValues = new ArrayList();
			 List<String> list = Arrays.asList(translation,page,limit,item_limit,translation,country,languages,epg_data);
		     queryValues.addAll(list);		    
		     HashMap<String, String> params = (HashMap<String, String>) queryParamsArray("gwapi_content_collectionapiJson", queryValues);
		     System.out.println(params);
		     
		     if (responseCode.contains("401")) {
		    	
					req = (FilterableRequestSpecification) given().spec(requestSpecification(BaseUrl_gwAPI));
				}else {
					 //Get Authorization
					  String 	authorization=getAuthorizationForEmailLogin(username, password);
		               req = (FilterableRequestSpecification) given().spec(requestSpecification(BaseUrl_gwAPI)).header("X-ACCESS-TOKEN",xAccessToken).header("Authorization", authorization).pathParams(params1).queryParams(params);
				}		
			response = req.when().get(getResource("gwapi_content_collectionapi_path")).then().extract().response();
			boolean status=false;
			String actStatusCode=Integer.toString(response.getStatusCode());
			 System.out.println(actStatusCode);
			 if((responseCode.contains(actStatusCode))) {
					if((actStatusCode.contains("400")&& response.asString().contains(errorCode) && response.asString().contains(message))||(actStatusCode.contains("401")&& response.asString().contains(errorCode) && response.asString().contains(message))||(actStatusCode.contains("404")&& response.asString().contains(errorCode) && response.asString().contains(message))) {
			 			 testLogger.log(Status.PASS,
									MarkupHelper.createLabel("Response Time(ms)is-"+response.getTimeIn(TimeUnit.MILLISECONDS), ExtentColor.GREEN));
						 LoggingUtils.info("Response Time(ms)is-"+response.getTimeIn(TimeUnit.MILLISECONDS));
						 testLogger.log(Status.PASS,
									MarkupHelper.createLabel("Expected Response code is returned-"+actStatusCode, ExtentColor.GREEN));

						 LoggingUtils.info("Expected Response code is returned-"+actStatusCode);
						Map<String, String> mapAPI = convertArrayAPIResponseToHashMap(response);
						compareJsonSchemaAndResponseBody(mapAPI, "gwapi_content_collectionapiJson", responseCode, "gwapi_content_collectionapi_path", softasrt);		    
						status=true;	
					}else if(actStatusCode.contains("200")) {
					 
					 testLogger.log(Status.PASS,
								MarkupHelper.createLabel("Response Time(ms)is-"+response.getTimeIn(TimeUnit.MILLISECONDS), ExtentColor.GREEN));
					 LoggingUtils.info("Response Time(ms)is-"+response.getTimeIn(TimeUnit.MILLISECONDS));
					 testLogger.log(Status.PASS,
								MarkupHelper.createLabel("Expected Response code is returned-"+actStatusCode, ExtentColor.GREEN));

					 LoggingUtils.info("Expected Response code is returned-"+actStatusCode);
					Map<String, String> mapAPI = convertAPIResponseToHashMap(response);
					compareJsonSchemaAndResponseBody(mapAPI, "gwapi_content_collectionapiJson", responseCode, "gwapi_content_collectionapi_path", softasrt);		    
					status=true;
					}
				 }
			 if(status) {
				// Json.XrayJsonImport(jira_id, "PASSED");
				 softasrt.assertEquals(status, true, "gwapi_content_collectionapi - PASSED");
					System.out.println("gwapi_content_collectionapi - PASSED");
					testLogger.log(Status.PASS,
							MarkupHelper.createLabel("gwapi_content_collectionapi: Expected Response code is returned and schema is mapping proerly.", ExtentColor.GREEN));
					 LoggingUtils.info("gwapi_content_collectionapi: Expected Response code is returned and schema is mapping proerly.");
					softasrt.assertAll();
					}else {
						 Json.XrayJsonImport(jira_id, "FAILED");
						System.out.println("gwapi_content_collectionapi - FAILED");
					testLogger.log(Status.FAIL,
							MarkupHelper.createLabel("gwapi_content_collectionapi - FAILED", ExtentColor.RED));
					LoggingUtils.error("gwapi_content_collectionapi - FAILED");
					softasrt.assertEquals(false, true, "gwapi_content_collectionapi - FAILED");					
					softasrt.assertAll();
					}
	/*	 }catch(Exception e) {
			 Json.XrayJsonImport(jira_id, "FAILED");
			testLogger.log(Status.FAIL,
					MarkupHelper.createLabel("gwapi_content_collectionapi - FAILED", ExtentColor.RED));
			LoggingUtils.error("gwapi_content_collectionapi - FAILED");
			softasrt.assertEquals(false, true, "gwapi_content_collectionapi - FAILED");					
			softasrt.assertAll();
			 
		 }
	  }*/
	

	 }

}
