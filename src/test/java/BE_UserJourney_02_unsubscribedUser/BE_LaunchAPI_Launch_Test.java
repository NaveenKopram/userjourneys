package BE_UserJourney_02_unsubscribedUser;

import static io.restassured.RestAssured.given;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

import commonFunctions.TestUtil;
import io.restassured.specification.FilterableRequestSpecification;

import resources.UserJourney02_TestData_DataProvider;

import utils.Json;
import utils.LoggingUtils;

public class BE_LaunchAPI_Launch_Test extends TestUtil {

	SoftAssert softasrt;
	
	 @Test(dataProviderClass = UserJourney02_TestData_DataProvider.class, dataProvider = "launchapi_Launch", description="Launch API", enabled = true)
	
	 public void launchapi_Launch(String responseCode, String schemaName, String errorCode, String message, String ccode, String lang, String fetch, String version, String lang_localized_order, String country, String state, String translation,String platform_name,String jira_id) throws IOException, InterruptedException {
		  
			   System.out.println("params");
			   testLogger=extent.createTest("launchapi_Launch :"+responseCode,"Test to Validate Response-"+responseCode+"</br>Test Data: </br>responseCode- "+responseCode+"</br>schemaName- "+schemaName+"</br>errorCode- "+errorCode+"</br>message- "+message+"</br>ccode- "+ccode+"</br>lang- "+lang+"</br>fetch-"+fetch+"</br>version-"+version+"</br>lang_localized_order-"+lang_localized_order+"</br>country-"+country +"</br>state-"+state+"</br>translation-"+translation+"</br>platform_name-"+platform_name);	
			testLogger.info("Call post LaunchAPI: launchapi_Launch</br>Endpoint url-"+getGlobalValue(BaseUrl_launchAPI)+getResource("launchapi_Launch_path"));
			LoggingUtils.info("Call post LaunchAPI: launchapi_Launch</br>Endpoint url-"+getGlobalValue(BaseUrl_launchAPI)+getResource("launchapi_Launch_path"));
			softasrt=new SoftAssert();
			
			 List<String> queryValues = new ArrayList();
			 List<String> list = Arrays.asList(ccode,lang,fetch,version,lang_localized_order,country,state,translation,platform_name);
		     queryValues.addAll(list);		    
		     HashMap<String, String> params = (HashMap<String, String>) queryParamsArray("launchapi_LaunchJson", queryValues);
		     System.out.println(params);
		     
		    req = (FilterableRequestSpecification) given().spec(requestSpecification(BaseUrl_launchAPI)).queryParams(params);
			
			response = req.when().post(getResource("launchapi_Launch_path")).then().extract().response();
			boolean status=false;
			String actStatusCode=Integer.toString(response.getStatusCode());
			 System.out.println(actStatusCode);
			 if((responseCode.contains(actStatusCode))) {
					if((actStatusCode.contains("404")&& response.asString().contains(errorCode) && response.asString().contains(message))||(actStatusCode.contains("400")&& response.asString().contains(errorCode) && response.asString().contains(message))||(actStatusCode.contains("401")&& response.asString().contains(errorCode) && response.asString().contains(message))) {
						 testLogger.log(Status.PASS,
									MarkupHelper.createLabel("Response Time(ms)is-"+response.getTimeIn(TimeUnit.MILLISECONDS), ExtentColor.GREEN));
						 LoggingUtils.info("Response Time(ms)is-"+response.getTimeIn(TimeUnit.MILLISECONDS));
						 testLogger.log(Status.PASS,
									MarkupHelper.createLabel("Expected Response code is returned-"+actStatusCode, ExtentColor.GREEN));
						 LoggingUtils.info("Expected Response code is returned-"+actStatusCode);
						 Map<String, String> mapAPI = convertAPIResponseToHashMap(response);
						compareJsonSchemaAndResponseBody(mapAPI, "launchapi_LaunchJson", responseCode, "launchapi_Launch_path", softasrt);		    
						status=true;	
					}else if(actStatusCode.contains("200")) {
					 
					 testLogger.log(Status.PASS,
								MarkupHelper.createLabel("Response Time(ms)is-"+response.getTimeIn(TimeUnit.MILLISECONDS), ExtentColor.GREEN));
					 LoggingUtils.info("Response Time(ms)is-"+response.getTimeIn(TimeUnit.MILLISECONDS));
					 testLogger.log(Status.PASS,
								MarkupHelper.createLabel("Expected Response code is returned-"+actStatusCode, ExtentColor.GREEN));
					 LoggingUtils.info("Expected Response code is returned-"+actStatusCode);
					 Map<String, String> mapAPI = convertAPIResponseToHashMap(response);
					compareJsonSchemaAndResponseBody(mapAPI, "launchapi_LaunchJson", responseCode, "launchapi_Launch_path", softasrt);		    
					status=true;
					}
				 }
			 if(status) {
			//	 Json.XrayJsonImport(jira_id, "PASSED");	
				 softasrt.assertEquals(status, true, "launchapi_Launch - PASSED");
					System.out.println("launchapi_Launch - PASSED");
					testLogger.log(Status.PASS,
							MarkupHelper.createLabel("launchapi_Launch: Expected Response code is returned and schema is mapping proerly.", ExtentColor.GREEN));
					LoggingUtils.info("launchapi_Launch: Expected Response code is returned and schema is mapping proerly.");
					softasrt.assertAll();
					}else {
				//		Json.XrayJsonImport(jira_id, "FAILED");
						System.out.println("launchapi_Launch - FAILED");
					testLogger.log(Status.FAIL,
							MarkupHelper.createLabel("launchapi_Launch - FAILED", ExtentColor.RED));
					LoggingUtils.error("launchapi_Launch - FAILED");
					softasrt.assertEquals(false, true, "launchapi_Launch - FAILED");					
					softasrt.assertAll();
					}
	
	  }
	
	
	

}
