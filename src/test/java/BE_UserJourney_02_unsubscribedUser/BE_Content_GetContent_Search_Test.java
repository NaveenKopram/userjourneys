package BE_UserJourney_02_unsubscribedUser;

import static io.restassured.RestAssured.given;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

import commonFunctions.TestUtil;
import io.restassured.specification.FilterableRequestSpecification;

import resources.UserJourney02_TestData_DataProvider;
import utils.Json;
import utils.LoggingUtils;

public class BE_Content_GetContent_Search_Test extends TestUtil{
	SoftAssert softasrt;
	
	 
	 @Test(dataProviderClass = UserJourney02_TestData_DataProvider.class, dataProvider = "gwapi_content_getcontent_search",description = "Search" ,enabled = true)
	  public void gwapi_content_getcontent_search(String responseCode, String schemaName, String errorCode, String message,String q ,String limit, String page, String country,String business_type,String translation, String countries,String free_trial,String dishtv_free_trial, String asset_type,String version,String type,String languages,String src,String kids_safe) throws IOException, InterruptedException {
	
		 testLogger=extent.createTest("gwapi_content_getcontent_search :"+responseCode,"Test to Validate Response-"+responseCode+"</br>Test Data: </br>responseCode- "+responseCode+"</br>schemaName- "+schemaName+"</br>errorCode- "+errorCode+"</br>message- "+message+"</br>q -"+q+"</br>limit-"+limit+"</br>page- "+page+"</br>country- "+country+"</br>business_type -"+business_type+"</br>translation - "+translation+"</br>countries - "+countries+"</br>free_trial -"+free_trial+"</br>dishtv_free_trial- "+dishtv_free_trial+"</br>asset_type-"+asset_type+"</br>version-"+version+"</br>type-"+type+"</br>languages-"+languages+"</br>src-"+src);		
			testLogger.info("Call Get gwapi Content GetContent Search API: gwapi_content_getcontent_search</br>Endpoint url-"+getGlobalValue(BaseUrl_gwAPI)+getResource("gwapi_content_getcontent_search_path"));
			LoggingUtils.info("Call Get gwapi Content GetContent Search API: gwapi_content_getcontent_search</br>Endpoint url-"+getGlobalValue(BaseUrl_gwAPI)+getResource("gwapi_content_getcontent_search_path"));
			softasrt=new SoftAssert();
			
			 List<String> queryValues = new ArrayList();
			 List<String> list = Arrays.asList(q,limit,page,country,business_type,translation,countries,free_trial,dishtv_free_trial,asset_type,version,type,languages,src,kids_safe);
		     queryValues.addAll(list);		    
		     HashMap<String, String> params = (HashMap<String, String>) queryParamsArray("gwapi_content_getcontent_searchJson", queryValues);
		     System.out.println(params);
		     
		     if (responseCode.contains("401")) {
					req = (FilterableRequestSpecification) given().spec(requestSpecification(BaseUrl_gwAPI));
				}else {
					  
		               req = (FilterableRequestSpecification) given().spec(requestSpecification(BaseUrl_gwAPI)).queryParams(params);
				}	
			response = req.when().get(getResource("gwapi_content_getcontent_search_path")).then().extract().response();
			boolean status=false;
			String actStatusCode=Integer.toString(response.getStatusCode());
			 System.out.println(actStatusCode);
			 if((responseCode.contains(actStatusCode))) {
					if((actStatusCode.contains("400")&& response.asString().contains(errorCode) && response.asString().contains(message))||(actStatusCode.contains("401")&& response.asString().contains(errorCode) && response.asString().contains(message))||(actStatusCode.contains("404")&& response.asString().contains(errorCode) && response.asString().contains(message))) {
						 testLogger.log(Status.PASS,
									MarkupHelper.createLabel("Response Time(ms)is-"+response.getTimeIn(TimeUnit.MILLISECONDS), ExtentColor.GREEN));
						 LoggingUtils.info("Response Time(ms)is-"+response.getTimeIn(TimeUnit.MILLISECONDS));
						 testLogger.log(Status.PASS,
									MarkupHelper.createLabel("Expected Response code is returned-"+actStatusCode, ExtentColor.GREEN));
						 LoggingUtils.info("Expected Response code is returned-"+actStatusCode);
						 Map<String, String> mapAPI = convertAPIResponseToHashMap(response);
						compareJsonSchemaAndResponseBody(mapAPI, "gwapi_content_getcontent_searchJson", responseCode, "gwapi_content_getcontent_search_path", softasrt);		    
						status=true;	
					}else if(actStatusCode.contains("200")) {
					 
					 testLogger.log(Status.PASS,
								MarkupHelper.createLabel("Response Time(ms)is-"+response.getTimeIn(TimeUnit.MILLISECONDS), ExtentColor.GREEN));
					 LoggingUtils.info("Response Time(ms)is-"+response.getTimeIn(TimeUnit.MILLISECONDS));
					 testLogger.log(Status.PASS,
								MarkupHelper.createLabel("Expected Response code is returned-"+actStatusCode, ExtentColor.GREEN));
					 LoggingUtils.info("Expected Response code is returned-"+actStatusCode);
					Map<String, String> mapAPI = convertAPIResponseToHashMap(response);
					compareJsonSchemaAndResponseBody(mapAPI, "gwapi_content_getcontent_searchJson", responseCode, "gwapi_content_getcontent_search_path", softasrt);		    
					status=true;
					}
				 }
			 if(status) {
			
				 softasrt.assertEquals(status, true, "gwapi_content_getcontent_search - PASSED");
					System.out.println("gwapi_content_getcontent_search - PASSED");
					testLogger.log(Status.PASS,
							MarkupHelper.createLabel("gwapi_content_getcontent_search: Expected Response code is returned and schema is mapping proerly.", ExtentColor.GREEN));
					 LoggingUtils.info("gwapi_content_getcontent_search: Expected Response code is returned and schema is mapping proerly.");
					softasrt.assertAll();
					}else {
					//	Json.XrayJsonImport(jira_id, "FAILED");
						System.out.println("gwapi_content_getcontent_search - FAILED");
					testLogger.log(Status.FAIL,
							MarkupHelper.createLabel("gwapi_content_getcontent_search - FAILED", ExtentColor.RED));
					LoggingUtils.error("gwapi_content_amp_tvshowszee5originalstranslation - FAILED");
					softasrt.assertEquals(false, true, "gwapi_content_getcontent_search - FAILED");					
					softasrt.assertAll();
					}
		
		 } 
		 
	  
	



}
