package BE_UserJourney_01_GuestUser;

import static io.restassured.RestAssured.given;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

import commonFunctions.TestUtil;
import io.restassured.specification.FilterableRequestSpecification;
import resources.UserJourney01_TestData_DataProvider;
import resources.UserJourney02_TestData_DataProvider;

import utils.Json;
import utils.LoggingUtils;

public class BE_LaunchAPI_CountryList_Test extends TestUtil {
	SoftAssert softasrt;
	
	 @Test(dataProviderClass = UserJourney01_TestData_DataProvider.class, dataProvider = "LaunchAPI_CountryList", description="Launch API-Country List", enabled = true)
	  public void LaunchAPI_CountryList(String responseCode, String schemaName, String errorCode, String message, String lang, String ccode, String version,String fetch,String jira_id) throws IOException, InterruptedException {
		
		 testLogger=extent.createTest("LaunchAPI_CountryList :"+responseCode,"Test to Validate Response-"+responseCode+"</br>Test Data: </br>responseCode- "+responseCode+"</br>schemaName- "+schemaName+"</br>errorCode- "+errorCode+"</br>message- "+message+"</br>lang - "+lang +"</br>ccode - "+ccode +"</br>version-"+version+"</br>fetch-"+fetch);		
			testLogger.info("Call post launch API Country list : LaunchAPI_CountryList</br>Endpoint url-"+getGlobalValue(BaseUrl_launchAPI)+getResource("LaunchAPI_CountryList_path"));
			LoggingUtils.info("Call post launch API Country list: LaunchAPI_CountryList</br>Endpoint url-"+getGlobalValue(BaseUrl_launchAPI)+getResource("LaunchAPI_CountryList_path"));
			softasrt=new SoftAssert();
			
			 List<String> queryValues = new ArrayList();
			 List<String> list = Arrays.asList(lang,ccode,version,fetch);
		     queryValues.addAll(list);		    
		     HashMap<String, String> params = (HashMap<String, String>) queryParamsArray("LaunchAPI_CountryListJson", queryValues);
		     System.out.println(params);
		     
		    req = (FilterableRequestSpecification) given().spec(requestSpecification(BaseUrl_launchAPI)).queryParams(params);
			
			response = req.when().post(getResource("LaunchAPI_CountryList_path")).then().extract().response();
			boolean status=false;
			String actStatusCode=Integer.toString(response.getStatusCode());
			 System.out.println(actStatusCode);
			 if((responseCode.contains(actStatusCode))) {
					if((actStatusCode.contains("404")&& response.asString().contains(errorCode) && response.asString().contains(message))||(actStatusCode.contains("400")&& response.asString().contains(errorCode) && response.asString().contains(message))||(actStatusCode.contains("401")&& response.asString().contains(errorCode) && response.asString().contains(message))) {
						 testLogger.log(Status.PASS,
									MarkupHelper.createLabel("Response Time(ms)is-"+response.getTimeIn(TimeUnit.MILLISECONDS), ExtentColor.GREEN));
						 LoggingUtils.info("Response Time(ms)is-"+response.getTimeIn(TimeUnit.MILLISECONDS));
						 testLogger.log(Status.PASS,
									MarkupHelper.createLabel("Expected Response code is returned-"+actStatusCode, ExtentColor.GREEN));
						 LoggingUtils.info("Expected Response code is returned-"+actStatusCode);
						 Map<String, String> mapAPI = convertAPIResponseToHashMap(response);
						compareJsonSchemaAndResponseBody(mapAPI, "LaunchAPI_CountryListJson", responseCode, "LaunchAPI_CountryList_path", softasrt);		    
						status=true;	
					}else if(actStatusCode.contains("200")) {
						 
					 
					 testLogger.log(Status.PASS,
							MarkupHelper.createLabel("Response Time(ms)is-"+response.getTimeIn(TimeUnit.MILLISECONDS), ExtentColor.GREEN));
					 LoggingUtils.info("Response Time(ms)is-"+response.getTimeIn(TimeUnit.MILLISECONDS));
					 testLogger.log(Status.PASS,
								MarkupHelper.createLabel("Expected Response code is returned-"+actStatusCode, ExtentColor.GREEN));
					 LoggingUtils.info("Expected Response code is returned-"+actStatusCode);
				//	 Map<String, String> mapAPI = convertAPIResponseToHashMap(response);
					Map<String, String> mapAPI = convertListAPIResponseToHashMap(response);
					System.out.println(mapAPI);
					compareJsonSchemaAndResponseBodyArray(mapAPI, "LaunchAPI_CountryListJson", responseCode, "LaunchAPI_CountryList_path", softasrt);		    
					status=true;
					} 
				 }
			 if(status) {
			//	 Json.XrayJsonImport(jira_id, "PASSED");
				 softasrt.assertEquals(status, true, "LaunchAPI_CountryList - PASSED");
					System.out.println("LaunchAPI_CountryList - PASSED");
					testLogger.log(Status.PASS,
							MarkupHelper.createLabel("LaunchAPI_CountryList: Expected Response code is returned and schema is mapping proerly.", ExtentColor.GREEN));
					LoggingUtils.info("LaunchAPI_CountryList: Expected Response code is returned and schema is mapping proerly.");
					softasrt.assertAll();
					}else {
					//	Json.XrayJsonImport(jira_id, "FAILED");
						System.out.println("LaunchAPI_CountryList - FAILED");
					testLogger.log(Status.FAIL,
							MarkupHelper.createLabel("LaunchAPI_CountryList - FAILED", ExtentColor.RED));
					LoggingUtils.error("LaunchAPI_CountryList - FAILED");
					softasrt.assertEquals(false, true, "LaunchAPI_CountryList - FAILED");					
					softasrt.assertAll();
					}  
		
	  }
	
}





