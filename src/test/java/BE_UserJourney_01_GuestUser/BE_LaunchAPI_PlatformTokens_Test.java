package BE_UserJourney_01_GuestUser;

import static io.restassured.RestAssured.given;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

import commonFunctions.TestUtil;
import io.restassured.specification.FilterableRequestSpecification;
import resources.UserJourney01_TestData_DataProvider;
import resources.UserJourney02_TestData_DataProvider;

import utils.Json;
import utils.LoggingUtils;

public class BE_LaunchAPI_PlatformTokens_Test  extends TestUtil {
	SoftAssert softasrt;
	//launchapi_PlatformTokensJson
	//launchapi_PlatformTokens_path
	//launchapi_PlatformTokens
	
	@Test(dataProviderClass = UserJourney01_TestData_DataProvider.class, dataProvider = "launchapi_PlatformTokens", description="Launch API - Platform Tokens", enabled = true)
	public void launchapi_PlatformTokens(String responseCode,String schemaName,String errorCode,String message,String platform_name,String jira_id)throws IOException, InterruptedException{
	
	
		testLogger=extent.createTest("launchapi_PlatformTokens :"+responseCode,"Test to Validate Response-"+responseCode +"</br>Test Data: </br>responseCode- "+responseCode+"</br>schemaName- "+schemaName+"</br>errorCode- "+errorCode+"</br>message- "+message+"</br>platform_name - "+platform_name);
		testLogger.info("Call get PlatformTokens: launchapi_PlatformTokens</br>Endpoint url-"+getGlobalValue(BaseUrl_launchAPI)+getResource("launchapi_PlatformTokens_path"));
		LoggingUtils.info("Call get PlatformTokens: launchapi_PlatformTokens</br>Endpoint url-"+getGlobalValue(BaseUrl_launchAPI)+getResource("launchapi_PlatformTokens_path"));
		softasrt=new SoftAssert();
		

		
		List<String> queryValues = new ArrayList();
		List<String> list = Arrays.asList(platform_name);
		queryValues.addAll(list);
		 HashMap<String, String> params = (HashMap<String, String>) queryParamsArray("launchapi_PlatformTokensJson", queryValues);
		 System.out.println(params);
		 
		 
		 
		 if (message.contains("Parameter not present"))
		 {
		  req = (FilterableRequestSpecification) given().spec(requestSpecification(BaseUrl_launchAPI));
		    		
		 }
		 else
		 {
			 req = (FilterableRequestSpecification) given().spec(requestSpecification(BaseUrl_launchAPI))
			    		.queryParams(params);
		 }
		 
		  
		  
		 response = req.when().get(getResource("launchapi_PlatformTokens_path")).then().extract().response();
		 boolean status=false;
		 String actStatusCode=Integer.toString(response.getStatusCode());
		 
		 System.out.println(actStatusCode);
		 if((responseCode.contains(actStatusCode))) {
				if((actStatusCode.contains("400")&& response.asString().contains(errorCode) && response.asString().contains(message))||(actStatusCode.contains("403")&& response.asString().contains(errorCode) && response.asString().contains(message))) {
					 testLogger.log(Status.PASS,
								MarkupHelper.createLabel("Response Time(ms)is-"+response.getTimeIn(TimeUnit.MILLISECONDS), ExtentColor.GREEN));
					 LoggingUtils.info("Response Time(ms)is-"+response.getTimeIn(TimeUnit.MILLISECONDS));
					 testLogger.log(Status.PASS,
								MarkupHelper.createLabel("Expected Response code is returned-"+actStatusCode, ExtentColor.GREEN));
					 LoggingUtils.info("Expected Response code is returned-"+actStatusCode);
					 Map<String, String> mapAPI = convertAPIResponseToHashMap(response);
					compareJsonSchemaAndResponseBody(mapAPI, "launchapi_PlatformTokensJson", responseCode, "launchapi_PlatformTokens_path", softasrt);		    
					status=true;
					
				}else if(actStatusCode.contains("200")) {
					 
					 testLogger.log(Status.PASS,
								MarkupHelper.createLabel("Response Time(ms)is-"+response.getTimeIn(TimeUnit.MILLISECONDS), ExtentColor.GREEN));
					 LoggingUtils.info("Response Time(ms)is-"+response.getTimeIn(TimeUnit.MILLISECONDS));
					 testLogger.log(Status.PASS,
								MarkupHelper.createLabel("Expected Response code is returned-"+actStatusCode, ExtentColor.GREEN));
					 LoggingUtils.info("Expected Response code is returned-"+actStatusCode);
					 Map<String, String> mapAPI = convertAPIResponseToHashMap(response);
					compareJsonSchemaAndResponseBody(mapAPI, "launchapi_PlatformTokensJson", responseCode, "launchapi_PlatformTokens_path", softasrt);		    
					status=true;
					}
				 }
		 if(status) {
			// 	Json.XrayJsonImport(jira_id, "PASSED");
				softasrt.assertEquals(status, true, "launchapi_PlatformTokens - PASSED");
				System.out.println("launchapi_PlatformTokens - PASSED");
				testLogger.log(Status.PASS,
						MarkupHelper.createLabel("launchapi_PlatformTokens: Expected Response code is returned and schema is mapping proerly.", ExtentColor.GREEN));
				LoggingUtils.info("launchapi_PlatformTokens: Expected Response code is returned and schema is mapping proerly.");
				softasrt.assertAll();
				}else {
			//	Json.XrayJsonImport(jira_id, "FAILED");
				System.out.println("launchapi_PlatformTokens - FAILED");
				testLogger.log(Status.FAIL,
						MarkupHelper.createLabel("launchapi_PlatformTokens - FAILED", ExtentColor.RED));
				LoggingUtils.error("launchapi_PlatformTokens - FAILED");
				softasrt.assertEquals(false, true, "launchapi_PlatformTokens - FAILED");					
				softasrt.assertAll();
				}
	 
	 
	}
	
	
	 
}
